%!TEX root = ./main2.tex
%
% guidance how to interpret them
% make a concluding section about what we learned
% explain why
% mentioning strange results is not bad.
%
% why are results better than others, what can we conclude.
% point the reader to the interesting bits and what's so interesting about it
%
% use the whole dataset for the results tables
%
% seeing this outcome, we make some extra experiments or referring to other people's work to justify the results.
% mention Sorec more
% take a look at result discussion of other papers
\chapter{Results and Discussion}
\label{ch:results}

We will now evaluate the work that has been implemented for our newly proposed methods and discuss our results in tandem with the results obtained by other reproduced state-of-the-art methods for our dataset.
The results are split in two Sections, one that addresses the rating prediction task and another which focuses on the item recommendation task directly.
We conclude this Chapter with a Section dedicated in qualitative analysis of the topic modeling using some examples drawn from our dataset.

\section{Rating Prediction}
    We first reproduce the simplistic approaches as well as some state-of-the-art methods that have been covered in Section \ref{sec:baselines_and_comparisons}.
    We compare the results we obtain for state-of-the-art methods to those achieved by incorporating social and textual information.

    The \textit{Global Average} and \textit{Majority} methods' performance is shown in the \textit{Naive Baselines} segment of Table \ref{tbl:result_overview}.
    As stated previously, those methods carry no intelligence and therefore only exist to provide an intuition for the contents of our ratings matrix and to present something to improve upon.
    We notice however, that despite their simplicity they seem to yield particularly good results on the dataset.

    The \textit{Global Average}'s performance clearly exceeds that of \textit{Majority}'s.
    The superior naive method achieves an impressive score below than $1.0$ on \textit{MAE} and very close to the $1.0$ mark for \textit{RMSE} which sets the bar high in terms of performance for the rest of the implemented methods.

    As discussed in Section \ref{sec:baselines_and_comparisons}, we applied a variety of machine learning algorithms to our dataset in order to provide the baseline performances that we want to improve upon.
    The results yielded by each of the baseline methods are outlined in the \textit{State-of-the-art} section of Table \ref{tbl:result_overview}.

    At a first glance we can see that the best performance is achieved by \textit{BiasedMF} for both metrics, by far outperforming all the rest.
    The second best performing method is \textit{RegSVD}.
    \textit{Probabilistic Matrix Factorization} has the worst performance out of the six methods yielding a poor $3.03$ for \textit{RMSE} and close to the $3$ mark again for \textit{MAE} which renders it the most unsuitable for the given experiment.

    Lastly for the task of rating prediction, we take a look at the performances yielded by the models we implemented.
    In Table \ref{tbl:result_overview} we can have a look at at the collective performances of all the methods we reproduced, with the last segment being dedicated to the newly introduced methods.
    As we saw in the analysis of the state-of-the-art methods, the method that seemed to outperform the rest was \textit{BiasedMF} which achieved top notch performance in both error metrics.

    \subsection{Social-enhanced recommendation}
        We evaluate three variations of \textit{SoRec}, all having the user-to-user trust weighted by the extracted similarity measure as described in Chapter \ref{ch:methodology}.
        Unlike \citet{Ma2008SoRecSR} who had an elaborate way of determining social trust through their platform, we determine trust using the similarity features described in Chapter \ref{ch:methodology}.
        We notice in the \textit{SoRec} experimentation that the best performance is achieved by the hybrid method which determines user trust by weighing in both the social network as well as the textual information.

        Looking at the optimally weighed \textit{Weighted Friends Average} as it turned out to be (see Figure \ref{fig:weighted_rmse}) with $w_1=0.25$, $w_2=0.75$, which takes advantage of the the social factor, we see that we achieve a great performance which turns out to outperform all the previously introduced methods by far in the case of \textit{MAE} and have a loss of $0.025696$ on \textit{RMSE} performance over the best performer \textit{BiasedMF}.
        The similarity between the two methods' performance can be explained by the fact that both those methods resort to the global average for rating prediction when addressing the cold-start problem.

        \textit{Weighted Friends Average} achieves improvement over all variations of \textit{SoRec} as well, proving that weighing friends ratings in, along with the \textit{Global Average} is a good formula to tackle the problem by maximally utilizing the wisdom of the crowds.

        \begin{table}[t!]
        \centering\small
        \begin{tabular}{l c c}
            \toprule
            Method               & \textbf{MAE}  & \textbf{RMSE} \\
            \midrule
            \multicolumn{3}{@{}l}{\emph{Naive Baselines}} \\
              Global Average     & $0.905581$    & $1.23809$ \\
              Majority           & $1.586550$    & $1.98263$ \\
            \multicolumn{3}{@{}l}{\emph{State-of-the-art}} \\
              PMF                & $2.662781$    & $2.890186$ \\
              BayesianPMF        & $0.966021$    & $1.159835$ \\
              BiasedMF           & $0.994302$    & $1.194550$ \\
              RegSVD             & $1.006511$    & $1.422791$ \\
            \midrule
            \multicolumn{3}{@{}l}{\emph{Proposed}} \\
              $sim_{social}$ SoRec                      & $1.217908$    & $1.413721$ \\
              $sim_{text}$ SoRec                        & $1.192209$    & $1.386090$ \\
              $sim_{hybrid}$ SoRec                      & $1.171317$    & $1.200289$ \\
              Friends Average ($w_1=0.25$, $w_2=0.75$)  & \bf{0.735320} & \bf{1.17047} \\
              Regression on $sim_{LDA}$                 & $1.194674$    & $1.387570$ \\
            \bottomrule
        \end{tabular}
        \caption[Rating prediction result overview]{Results of methods aimed at rating prediction}
        \label{tbl:result_overview}
        \end{table}

    \subsection{Text-enhanced recommendation}
        The second method we present is the \textit{LDA Similarity based Regression}, in this method we make use of the features extracted from the \textit{LDA} similarity of the comments to predict the ratings.
        As presented in the table, a simple regression over the carefully selected \textit{LDA} features extracted makes for a very effective implementation which can easily stand very close to the advanced mathematical models that are used by the State of the art methods presented previously.

    \subsection{Discussion}
        Reading the table, we notice some interesting facts, starting with the naive baselines.
        In our case, the naive baseline results can serve as baselines but an interesting aspect for justifying them could be social bias.
        In their work \citet{Wang_1social} investigate the social influence in user generated content on online rating websites and they conclude that such influence exists and can greatly account for homophily in online book ratings.
        They proceed to quantize the influences based on the popularity of rated items, the experience of the user, the size of the users' social networks and other factors.
        As is in the previously mentioned in the yelp platform, users are exposed to the average ratings of businesses before they determine theirs, and that is highly likely to contribute in the high performance of a method as simple as \textit{Global Average}.

        In the state-of-the-art methodology segment, we can assert \citet{Salakhutdinov08bayesianprobabilistic}'s claim that Bayesian Probabilistic Matrix Factorization models can be efficiently trained using Markov chain Monte Carlo methods with a significantly higher prediction score than PMF models using Maximum a posteriori estimation, as this method outperforms all of the rest of that table section's occupants.

        Lastly for our methods we notice that of the three implemented \textit{SoRec} variations, the hybrid which incorporates both textual and social information achieves the best score.
        This goes to say that using the combination of both text and social features achieves a finer grained user-to-user trust measure than using any of the two separately.
        All of the \textit{SoRec} implementations though as well as the state-of-the-art methods fall short compared to the optimally weighed \textit{Friends Average} method.

        To sum up, this method's success can be greatly explained by the performance of the \textit{Global Average} method.
        Essentially what our method does is incorporate the good performance of \textit{Global Average} and learn the optimal weighing scheme with which to exploit the wisdom of the crowds.

\section{Item Recommendation}
    Having seen the encouraging output of our feature selection and evaluation against rating prediction we proceed to further capitalize on it by putting it to the test on the item recommendation scenario.
    In this Subsection, the goal changes from rating prediction, to item list generation.

    We will experiment on feature selection and evaluate how different learning to rank algorithms perform on those features.
    The similarity features that we extract from the corpus are: $sim_{w2v}$, $sim_{LDA}$, $sim_{social}$ and $sim_{hybrid}$ as discussed in Chapter \ref{ch:methodology}.

    The algorithms we experimented with for this task are: \textit{LambdaMART}, \textit{Random Forests} and \textit{Coordinate Ascent}.
    As mentioned in Chapter \ref{ch:methodology} the method we picked to report on is \textit{LambdaMART}.

    \subsection{Text-enhanced recommendation}
        We start off by questioning the performance gain obtained from the textual information to answer Research Question \ref{rq:text}.
        For this task we train our learning to rank algorithm using the features $sim_{w2v}$ and $sim_{LDA}$.
        Table \ref{tbl:text_item_rec} illustrates the results obtained while experimenting using the textual information.

        \begin{table}[t!]
        \centering\small
        \begin{tabular}{l c}
        \toprule
        Feature Selection               & \textbf{NDCG@10} \\
        \midrule
            $sim_{w2v}$                 & $0.7339$ \\
            $sim_{LDA}$                 & $0.7682$ \\
            $sim_{LDA}$ + $sim_{w2v}$   & \bf{0.8039} \\
        \bottomrule
        \end{tabular}
        \caption[Item recommendation addressing RQ\ref{rq:text}]{RQ\ref{rq:text}: \textit{NDCG@10} for item recommendation.}
        \label{tbl:text_item_rec}
        \end{table}

        The results show that when used solely, feature $sim_{LDA}$ outperforms $sim_{w2v}$.
        As expected, when both features are used the resulting model generates a better output.

    \subsection{Hybrid recommendation}
        Next, we incorporate the social aspect in order to boost the results obtained in the previous experiments.
        The social feature we extract is characterized by a weighting pattern according to which we weigh the user text vector over the social circle's text vectors as discussed in Section \ref{sec:hybrid_recommendation}.
        Extracting those features took an abundance of time to complete therefore it wasn't feasible to exhaustingly go through all weighting combinations as we would have wanted.
        We did however pick some, that collectively with the previously yielded results seem to provide answers to Research Question \ref{rq:social}.

        For brevity we consider the following weighting schemes: $[0.1,0.9]$, $[0.2,0.8]$, $[0.5,0.5]$, $[0.8,0.2]$ and $[0.9,0.1]$, therefore selecting two scenarios favoring the social circle over the user's text vector, one that is balanced between the two, and another two that lightly incorporates the social circle vector.
        In Table \ref{tbl:social_weighting_comparison} we present the results yielded, grouped by weighting scheme.

        \begin{table}[h!]
        \centering\small
        \begin{tabular}{l c}
        \toprule
        Feature Selection                             & \textbf{NDCG@10} \\
        \midrule
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.1, 0.9{]}}} \\
            $sim_{hybrid w2v}$                        & $0.7637$ \\
            $sim_{hybrid LDA}$                        & $0.7906$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.8003$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.2, 0.8{]}}} \\
            $sim_{hybrid w2v}$                        & $0.7534$ \\
            $sim_{hybrid LDA}$                        & $0.8422$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.8225$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.5, 0.5{]}}} \\
            $sim_{hybrid w2v}$                        & $0.9153$ \\
            $sim_{hybrid LDA}$                        & $0.9178$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.9192$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.8, 0.2{]}}} \\
            $sim_{hybrid w2v}$                        & \bf{0.9175} \\
            $sim_{hybrid LDA}$                        & \bf{0.9179} \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & \bf{0.9221} \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.9, 0.1{]}}} \\
            $sim_{hybrid w2v}$                        & $0.8991$ \\
            $sim_{hybrid LDA}$                        & $0.9168$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.9154$ \\
        \bottomrule
        \end{tabular}
        \caption[Item recommendation addressing RQ\ref{rq:social}]{RQ\ref{rq:social}: \textit{NDCG@10} for item recommendation.}
        \label{tbl:social_weighting_comparison}
        \end{table}

        We see that the hybrid method's results which mainly consist of the user text vector based similarity but also incorporate the social vector achieve the best performance for the most part.
        Namely, the social weighing scheme of ${[}0.8, 0.2{]}$ outperforms all of the rest showing us that the silver lining lies somewhere close to what our regression showed us in previous experiments.
        Feature $sim_{hybrid LDA}$ proves to be a better performer for text vectorization, outperforming word2vec for all of the experiments.
        Using the combination of both of those features scores higher than using them separately which is to be expected since a richer feature vector can have that effect.

    \subsection{Proof of concept on a denser dataset}
        In order to investigate the results obtained in this segment further, we proceed to test a the previous features on a different subset of the dataset as a proof of concept for our methodology.
        For the purpose of this experiment, we consider the part of the data that is less prone to the cold-start problem in order to see how well our method performs if cold-start was not a factor.
        We therefore select the users who have made at least $10$ text reviews\footnote{As presented in Section \ref{sec:dataset} the average review count per user is 24.}.
        We hope to see an improvement over the previous results since few rating users are eliminated in this case.
        Table \ref{tbl:dense} shows the scores obtained for the denser subset alongside the divergence from the results on the whole dataset.

        \begin{table}[!ht]
        \centering\small
        \begin{tabular}{l c c}
        \toprule
        Feature Selection                             & \textbf{NDCG@10} & \textbf{Difference}\\
        \midrule
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.1, 0.9{]}}} \\
            $sim_{hybrid w2v}$                        & $0.7632$    & $\triangledown 0.0005$ \\
            $sim_{hybrid LDA}$                        & $0.8056$    & $\blacktriangle 0.0015$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.8003$    & $\triangledown 0.0008$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.2, 0.8{]}}} \\
            $sim_{hybrid w2v}$                        & $0.7544$    & $\blacktriangle 0.0010$ \\
            $sim_{hybrid LDA}$                        & $0.8420$    & $\triangledown 0.0002$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.8233$    & $\blacktriangle 0.0008$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.5, 0.5{]}}} \\
            $sim_{hybrid w2v}$                        & $0.9152$    & $\triangledown 0.0001$ \\
            $sim_{hybrid LDA}$                        & \bf{0.9180} & $\blacktriangle 0.0002$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.9199$    & $\blacktriangle 0.0007$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.8, 0.2{]}}} \\
            $sim_{hybrid w2v}$                        & \bf{0.9185} & $\blacktriangle 0.0010$ \\
            $sim_{hybrid LDA}$                        & $0.9177$    & $\blacktriangle 0.0008$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & \bf{0.9236} & $\blacktriangle 0.0015$ \\
        \multicolumn{2}{@{}l}{\emph{Social weighting: {[}0.9, 0.1{]}}} \\
            $sim_{hybrid w2v}$                        & $0.8988$    & $\triangledown 0.0003$ \\
            $sim_{hybrid LDA}$                        & $0.9169$    & $\blacktriangle 0.0001$ \\
            $sim_{hybrid LDA}$ + $sim_{hybrid w2v}$   & $0.9151$    & $\triangledown 0.0003$ \\
        \bottomrule
        \end{tabular}
        \caption[Item recommendation results for dense data subset]{Item recommendation results for dense data subset opposed to the entire dataset}
        \label{tbl:dense}
        \end{table}

        What we see in these results is a slight overall improvement over the previous table.
        The highest improvement is noticed in the ${[}0.8, 0.2{]}$ weighting scheme, the one that achieves the overall best results.
        These results show that our methods performance increases when the dataset involves more textual reviews, which is an encouraging outcome as a proof of concept.

        To sum up, for item recommendation our topic modeling that utilizes the social aspect in order to refine the user vector achieves a better performance than the one using word2vec and the denser the dataset, the higher performance is achieved.

\section{Textual model qualitative analysis}
\label{sec:text_analusis}
    In this section we will take a closer look on how our textual related models perform for some example user-documents.

    LDA is more intuitive to investigate since it outputs topic models.
    We pick 4 of the 50 topics extracted to study how fine grained the topics are.
    Figure \ref{fig:lda_topics} depicts those topics (the sizes of the words show their likelihood per topic).
    Subfigure \ref{fig:wc1} captures a topic that includes fast food places, \ref{fig:wc2} captures restaurants ideal for dates, \ref{fig:wc3} illustrates family friendly businesses and \ref{fig:wc4} shows businesses that can be characterized as sports bars.

    \begin{figure}[!htb]
    \centering
    \subfigure[]{\label{fig:wc1}\includegraphics[width=0.44\textwidth]{img/topics/t1.png}}
    \subfigure[]{\label{fig:wc2}\includegraphics[width=0.44\textwidth]{img/topics/t2.png}}
    \subfigure[]{\label{fig:wc3}\includegraphics[width=0.44\textwidth]{img/topics/t3.png}}
    \subfigure[]{\label{fig:wc4}\includegraphics[width=0.44\textwidth]{img/topics/t4.png}}
    \caption{LDA topics}
    \label{fig:lda_topics}
    \end{figure}

    Most of the rest of the topics follow suit with some of them capturing themes such as: Greek restaurants (\rm{\it{``pita'', ``Greek'', ``Mediterranean'', ``feta''}} etc.), Drive-through restaurants (\rm{\it{``drive'', ``fast'', ``quick'', ``window''}} etc.), food markets (\rm{\it{``farmer'', ``market'', ``vegan'', ``organic'', ``healthy''}} etc.).
    This demonstrates at a first glance that LDAs output manages to capture business types well.
    Therefore, the rest of the process to determine user taste as a mixture of probabilities of users adhering to those topics makes LDA a valid choice.

    Using the topics illustrated in \ref{fig:lda_topics}, we investigate the outputted similarity features using four example user-documents from our dataset.
    In Table \ref{tbl:userdocuments} we show the 4 handpicked user-documents stripped down to the most frequent topic terms.
    Red fonts correspond to words from Topic a, green fonts correspond to Topic b, blue fonts correspond to Topic c and fonts to Topic d.
    The pairs we expect to see matching are User \#1 to Topic a, User \#2 to Topic b, User \#3 to Topic c and User \#4 to Topic d.

    Table \ref{tbl:lda_outputs} shows the probability of each topic (out of the 50 topics) given the user documents.
    We can see the diagonal heavy output that we expected to see, which confirms the hypothesis we expressed previously.
    LDA manages to successfully capture the themes that users have expressed in their previous comments.

    To summarize, LDA provides a good solution for capturing concise themes of businesses as illustrated in Figure \ref{fig:lda_topics}.
    This could be useful for building context in scenarios where items are accompanied by user comments.
    Further, using the topic models extracted we manage to successfully capture user inclination towards specific types of topics and therefore businesses, by observing their review texts.

    \begin{tabularx}{\textwidth}{X}
    \toprule
    User-document (\#1), document length\footnote{in words}: 4382 \\
    \midrule  % JCfHBL4zGkPjtKVd9hS_Bw
    \rm{ \it{(...) went happi hour got \textcolor{rred}{burger} beer 10 consid size \textcolor{rred}{burger} az local beer san tan \textcolor{ppurple}{breweri} pretti impress order \textcolor{rred}{bacon} \textcolor{rred}{burger} pile shroom drip havarti chees \textcolor{rred}{burger} anoth categori \textcolor{rred}{bun} soft yet toast great pretzel flavor wait guess asu student nice attent made sure everyth need back tackl rest \textcolor{rred}{burger} yuppi interior decor meet mexican food blanco food \textcolor{rred}{chees} crisp good peopl watch best part singl male look anoth male stomach bland frooffi food place guess five star tofu dish look yummi flagstaff close vegi food two dish far excel order mongolian \textcolor{rred}{beef beef} sub tofu sesam chicken chicki sub tofu make place stand 99 percent \textcolor{rred}{bun} tofu \textcolor{rred}{fri bun fri} ecstat one note get tofu dish recommend dine bread usual overcook would \textcolor{ggreen}{ambianc} nice normal much expect strip mall restaur brother cousin meat dish}} \\

    \midrule
    User-document (\#2), document length: 448 \\
    \midrule  % wsouO79E-BW7lStwopkfjA
    \rm{ \it{(...) bar side portland although talk \textcolor{ggreen}{wine} bar actual includ fine assort scotch alway popular choic busi type \textcolor{ggreen}{wine} bar extens varieti solid name mani boutiqu \textcolor{ggreen}{wine} includ joel gott \textcolor{ggreen}{wine} work among other realli like bethg commit commun grown phoenix michel buy much fare possibl local schreiner sausag four peak \textcolor{ppurple}{breweri} local supplier addit vendor support local artist rotat thought provok \textcolor{ggreen}{date} work visit dylan michel great place one make downtown better offer bit distinct \textcolor{ggreen}{bottl} next time wonder fine meal great \textcolor{ggreen}{ambianc} (...)}} \\

    \midrule
    User-document (\#3), document length: 1495 \\
    \midrule  % lkV_WCqCbpA5gUIHD6wtOw
    \rm{ \it{ (...) \textcolor{bblue}{daughter} attend sinc 4 year old \textcolor{bblue}{love} teacher divers program offer 4th grade imagin leav go anywher down \textcolor{bblue}{famili} would alway met howev note salad gotten dinner \textcolor{bblue}{kid parent} food great favorit rigatoni order anyth els know great stuff love order dinner great \textcolor{bblue}{kid} love get ice cream latt bagel usual go one good \textcolor{ppurple}{friend} drop \textcolor{bblue}{kid} \textcolor{bblue}{school} \textcolor{bblue}{daughter mom} went insid muffin left buy sweet potato chip bring home thing like anoth part heaven hide home offic know wrong say love went \textcolor{ppurple}{friend} (...)}} \\

    \midrule
    User-document (\#4), document length: 124 \\
    \midrule  % K4jiuwpunwXd6vniQgQ4vg
    \rm{ \it{(...) late \textcolor{ppurple}{night} \textcolor{ppurple}{drink} cool crowd place enter thru back never even went insid wait go back next time m town server quick kind \textcolor{ppurple}{drink} cold cheap patron cute fun re arizona gotta take advantag great weather enjoy \textcolor{ppurple}{cocktail} outdoor sun goe recent trip temp join \textcolor{ppurple}{friend} numer bar mill ave fun fill parti weekend \textcolor{ppurple}{tap craft beer} flow like \textcolor{ggreen}{wine} place best time beauti women instinct flock like bar towner take mine worth say ll sure \textcolor{ppurple}{drink} next time m town also left debit card open bar (...)}} \\
    \bottomrule
    \label{tbl:userdocuments}
    \end{tabularx}
    \captionof{table}{Example user-documents}

    \begin{center}
    \begin{tabular}[!htb]{c|cccc}
                                & User \#1          & User \#2          & User \#3          & User \#4 \\
        \hline
        $P(a|user-document)$    & \bf{0.0012845184} & 0.0009620618      & 0.0008706257      & 0.0006988574 \\
        $P(b|user-document)$    & 0.0008945980      & \bf{0.0016609629} & 0.0014659593      & 0.0009935127 \\
        $P(c|user-document)$    & 0.0015670780      & 0.0015624960      & \bf{0.0017335482} & 0.0016222612 \\
        $P(d|user-document)$    & 0.0014287056      & 0.0015050883      & 0.0030164696      & \bf{0.0042665175}
    \label{tbl:lda_outputs}
    \end{tabular}
    \captionof{table}{Feature outputs for example user-documents}
    \end{center}