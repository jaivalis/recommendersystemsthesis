from database.dbio import DB
import nltk.data
import matplotlib.pyplot as plt
import numpy as np

sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')


def count_words(text):
    return len(text.split())


def count_sentences(text):
    return len(sent_detector.tokenize(text.strip()))


def get_average_word_and_sentence_count():
    yelp = DB()

    word_count = 0
    sentence_count = 0

    review_count = 0
    for review in yelp.get_review_text_list():

        t = review['text']

        word_count += count_words(t)
        sentence_count += count_sentences(t)

        review_count += 1

    print 'average word count\t{}'.format(word_count / review_count)
    print 'average sentence count\t{}'.format(sentence_count / review_count)


def get_average_review_counts():
    yelp = DB()

    users = yelp.get_user_list()
    businesses = yelp.get_business_list()
    
    u_reviews = 0
    b_reviews = 0
    for u in users:
        u_reviews += u['review_count']
    for b in businesses:
        b_reviews += b['review_count']

    print 'average review count per user\t{}'.format(u_reviews / len(users))
    print 'average review count per business\t{}'.format(b_reviews / len(businesses))


def get_average_review_text_length_per_rating_histogram():
    yelp = DB()

    lengths = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}
    review_counts = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0}

    for r in yelp.get_review_list():
        s = r['stars']
        l = count_words(r['text'])

        lengths[s] += l
        review_counts[s] += 1

    x = np.array([1, 2, 3, 4, 5])
    y = [lengths[i]/review_counts[i] for i in lengths]
    # y = [0] + y

    print x
    print y
    bar_width = 0.65
    plt.bar(x-bar_width/2, y, bar_width)
    plt.xlabel('Score (stars)')
    plt.ylabel('Average review length (in tokens)')
    plt.show()


def get_user_item_rating_matrix_stats():
    yelp = DB()

    users = yelp.get_user_list()
    businesses = yelp.get_business_list()

    u_reviews = 0
    b_reviews = 0

    min_user = 100
    max_user = -1
    for u in users:
        u_reviews += u['review_count']
        min_user = min(min_user, u['review_count'])
        max_user = max(max_user, u['review_count'])
    min_item = 100
    max_item = -1
    for b in businesses:
        b_reviews += b['review_count']
        min_item = min(min_item, b['review_count'])
        max_item = max(max_item, b['review_count'])

    print 'Min. number of rated user: {}'.format(min_user)
    print 'Max. number of rated user: {}'.format(max_user)
    print 'Min. number of rated item: {}'.format(min_item)
    print 'Max. number of rated item: {}'.format(max_item)
    print 'average review count per user\t{}'.format(u_reviews / len(users))
    print 'average review count per business\t{}'.format(b_reviews / len(businesses))

if __name__ == '__main__':
    print '-'
    # get_average_word_and_sentence_count()
    get_average_review_counts()
    print '-'
    get_average_review_text_length_per_rating_histogram()
    # get_user_item_rating_matrix_stats()
