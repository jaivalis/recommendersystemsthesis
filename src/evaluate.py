import os
from operator import methodcaller
import collections
import matplotlib.pyplot as plt

import numpy as np
from tabulate import tabulate
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error

PREDICTIONS_FOLDER = './output/predictions/'


def sample(predictions, targets, n):
    """ Returns a sample of size n of a given list
    :param lst:
    :param n:
    :return:
    """
    A = np.concatenate(([predictions], [targets]), axis=0).T
    ret = A[np.random.choice(A.shape[0], n, replace=False), :]
    return ret[:, 0], ret[:, 1]  # predictions, targets


def rmse(predictions, targets):
    """ Root Mean Squared Error
    :param predictions: np.array
    :param targets: np.array
    :return: float
    """
    return np.sqrt(mean_squared_error(targets, predictions))


def mae(predictions, targets):
    """ Mean Average Error
    :param predictions: np.array
    :param targets: np.array
    :return: float
    """
    return mean_absolute_error(targets, predictions)


def parse_prediction_file(file_path):
    """ Returns two np.arrays
    :param file_path: a file to parse
    :return: true_label_array, prediction_array
    """
    with open(file_path, 'r') as f:
        trues = []
        predictions = []
        for line in f:
            (true_s, prediction_s) = line.rstrip().split('\t')
            trues.append(int(true_s))
            predictions.append(int(prediction_s))
    return np.array(trues), np.array(predictions)


def visualize_weighted_rmse(folder_path):
    """ Generates a plot containing the mean error (line) and the minimum to maximum span
    :param folder_path: path to the prediction files
    """
    pred_files = filter(methodcaller('endswith', '.pred'), os.listdir(folder_path))

    y_mean_dict = {}  # pyplot mean
    y_mins_dict = {}  # pyplot fill from
    y_maxs_dict = {}  # pyplot fill to
    for ff in pred_files:
        f_path = folder_path + ff
        w1 = ff.split('(')[1].split('-')[0]

        targets, predictions = parse_prediction_file(f_path)

        sample_count = 60
        for s in range(sample_count):  # sample n times
            predictions_sample, targets_sample = sample(predictions, targets, predictions.size / 8)
            error = rmse(targets_sample, predictions_sample)

            # update y_mean_dict, y_mins, y_maxs
            if s == 0:
                y_mins_dict[w1] = error
                y_maxs_dict[w1] = error
                y_mean_dict[w1] = error / float(sample_count)
            else:
                y_mean_dict[w1] += error / float(sample_count)
                y_mins_dict[w1] = min(error, y_mins_dict[w1])
                y_maxs_dict[w1] = max(error, y_maxs_dict[w1])

    y_mean_dict = collections.OrderedDict(sorted(y_mean_dict.items()))
    y_mins_dict = collections.OrderedDict(sorted(y_mins_dict.items()))
    y_maxs_dict = collections.OrderedDict(sorted(y_maxs_dict.items()))

    labels = []
    y = []
    y_min = []
    y_max = []
    for k in y_mean_dict:
        labels.append('({},{})'.format(k, 1-float(k)))
        y.append(y_mean_dict[k])
        y_min.append(y_mins_dict[k])
        y_max.append(y_maxs_dict[k])

    x = np.arange(.01, 1, .01)
    plt.plot(x, y, color='grey')
    plt.fill_between(x, y_min, y_max, facecolor='green', alpha=0.5)
    plt.xlabel('$w_1$')
    plt.ylabel('RMSE')
    plt.show()


def evaluate_independent(folder_path, prediction_files):
    res = [['Filename', 'RMSE', 'MAE']]

    # progress_bar = ProgressBar(maxval=len(prediction_files)).start()
    for e, ff in enumerate(prediction_files):
        # progress_bar.update(e + 1)

        f_path = folder_path + ff
        trues, predictions = parse_prediction_file(f_path)

        _rmse = rmse(trues, predictions)
        _mae = mae(trues, predictions)
        _fname = ff.split('.pred')[0]

        res.append([_fname, _rmse, _mae])
    # progress_bar.finish()
    print tabulate(res[1:], headers=res[0])


def evaluate_n_fold(folder_path, prediction_folders):
    res = [['Method_name', 'RMSE', 'MAE']]

    for sub_folder in prediction_folders:
        avg_rmse, avg_mae = 0, 0

        contents = filter(methodcaller('endswith', '.pred'), os.listdir(folder_path + sub_folder))
        for e, ff in enumerate(contents):
            f_path = folder_path + sub_folder + '/' + ff
            trues, predictions = parse_prediction_file(f_path)

            m = mae(trues, predictions)
            r = rmse(trues, predictions)
            avg_rmse += r 
            avg_mae += m
            res.append([f_path, r, m])

        res.append([sub_folder, avg_rmse / (e+1), avg_mae / (e+1)])
        # assert e == 4
    print tabulate(res[1:], headers=res[0])


def evaluate_all(folder_path):
    """ Prints the RMSE and MAE scores for all the files in a given folder
    :param folder_path: path to the prediction files
    """
    prediction_files = filter(methodcaller('endswith', '.pred'), os.listdir(folder_path))
    prediction_folders = filter(methodcaller('endswith', 'fold'), os.listdir(folder_path))

    evaluate_n_fold(folder_path, prediction_folders)
    print ''
    evaluate_independent(folder_path, prediction_files)

if __name__ == '__main__':
    evaluate_all(PREDICTIONS_FOLDER)
    # visualize_weighted_rmse(PREDICTIONS_FOLDER)

