from database.dbio import DB


def write_rating_line(f, u_id, b_id, stars):
    f.write('{} {} {}\n'.format(u_id, b_id, stars))


def serialize_ratings():
    """ Writes the true labels to file """
    yelp = DB()
    with open('../output/ratings.txt', 'w') as f:
        for review in yelp.get_review_list():
            u_id = review['user_id'].encode("ascii", "ignore")
            b_id = review['business_id'].encode("ascii", "ignore")
            stars = review['stars']

            write_rating_line(f, u_id, b_id, stars)


def serialize_ratings_numeric():
    """ Writes the true labels to file. Replaces the hash ids with numeric ids and writes them to separate files
    Necessary for the ListCF.
    user_id[TAB]item_id[TAB]rating[\n]
    """
    yelp = DB()
    u_map, i_map = {}, {}
    u_index, i_index = 1, 1
    with open('../output/true_labels/ratings.txt', 'w') as rf, \
            open('../output/true_labels/user_map.txt', 'w') as user_f, \
            open('../output/true_labels/item_map.txt', 'w') as item_f:

        for review in yelp.get_review_list():
            u_id = review['user_id'].encode("ascii", "ignore")
            i_id = review['business_id'].encode("ascii", "ignore")
            stars = review['stars']

            if u_id not in u_map:
                u_map[u_id] = u_index
                u_index += 1

            if i_id not in i_map:
                i_map[i_id] = i_index
                i_index += 1

            rf.write('{}\t{}\t{}\n'.format(u_map[u_id], i_map[i_id], stars))
        for k in u_map:
            user_f.write('{}\t{}'.format(k, u_map[k]))
        for k in i_map:
            item_f.write('{}\t{}'.format(k, i_map[k]))


if __name__ == '__main__':
    # serialize_ratings()
    serialize_ratings_numeric()
