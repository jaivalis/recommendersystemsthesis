from database.dbio import DB
from progressbar import *
from w2v import load_w2v_model, get_text_w2v_vector
import numpy as np
from util.my_math import matrix_column_mean


def update_business_review_text_12(database, w2v, topic_count):
    reviews = database.get_business_review_text_12_list()

    bar = ProgressBar(len(reviews), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

    for e, r in enumerate(reviews):
        bar.update(e+1)
        text = r['text']
        item_id = r['item_id']
        v = get_text_w2v_vector(w2v, text)

        database.set_w2v_vector('business_review_text_12', item_id, 'item_id', 'w2v@' + str(topic_count), v)
    bar.finish()


def update_business_review_text_45(database, w2v, topic_count):
    reviews = database.get_business_review_text_45_list()

    bar = ProgressBar(len(reviews), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

    for e, r in enumerate(reviews):
        bar.update(e+1)
        text = r['text']
        item_id = r['item_id']
        v = get_text_w2v_vector(w2v, text)

        database.set_w2v_vector('business_review_text_45', item_id, 'item_id', 'w2v@' + str(topic_count), v)
    bar.finish()


def update_user_review_text_1245(database, w2v, topic_count):
    user_ids = database.get_user_id_list()
    bar = ProgressBar(len(user_ids), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

    for e, u in enumerate(user_ids):
        bar.update(e+1)
        u_id = u['user_id']

        review_text_12 = database.get_user_review_text_12(u_id)
        review_text_45 = database.get_user_review_text_45(u_id)

        if review_text_12 is not None:
            v = get_text_w2v_vector(w2v, review_text_12)
            database.set_w2v_vector('user_review_text_12', u_id, 'user_id', 'w2v@' + str(topic_count), v)
        if review_text_45 is not None:
            v = get_text_w2v_vector(w2v, review_text_45)
            database.set_w2v_vector('user_review_text_45', u_id, 'user_id', 'w2v@' + str(topic_count), v)
    bar.finish()


def insert_friends_w2v(database, topic_count):
    user_ids = database.get_user_id_list()
    bar = ProgressBar(len(user_ids), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

    for e, u in enumerate(user_ids):
        bar.update(e+1)
        u_id = u['user_id']

        tmp_12, tmp_45 = None, None

        fr_count = 0
        for f_id in database.get_friends(u_id):

            v_12 = database.get_w2v_vector('user_review_text_12', f_id, 'user_id', topic_count)
            v_45 = database.get_w2v_vector('user_review_text_45', f_id, 'user_id', topic_count)

            if v_12 is not None:
                tmp_12 = np.vstack([tmp_12, v_12]) if tmp_12 is not None else v_12
            if v_45 is not None:
                tmp_45 = np.vstack([tmp_45, v_45]) if tmp_45 is not None else v_45

            fr_count += 1

        if tmp_12 is not None:
            tmp_12 = matrix_column_mean(tmp_12, axis=0)  # vertical
            database.set_w2v_vector('user_review_text_12', u_id, 'user_id',
                                    'social_w2v@' + str(topic_count), tmp_12)

        if tmp_45 is not None:
            tmp_45 = matrix_column_mean(tmp_45, axis=0)  # vertical
            database.set_w2v_vector('user_review_text_45', u_id, 'user_id',
                                    'social_w2v@' + str(topic_count), tmp_45)
    bar.finish()


if __name__ == '__main__':
    db = DB()
    topics = 50
    model = load_w2v_model('../output/w2v_models/model_7000_' + str(topics))

    # update_business_review_text_12(db, model, topics)
    # update_business_review_text_45(db, model, topics)

    # update_user_review_text_1245(db, model, topics)

    insert_friends_w2v(db, topics)
