from database.dbio import DB


db = DB()

with open('../output/social_connections.txt', 'w') as f:
    for user in db.get_user_list():
        u_id = user['user_id']

        friends = user['friends']

        f.write('{}\t{}\n'.format(u_id, ','.join(friends)))


