from database.dbio import DB
from data_exploration.serialize_text import process_text
from progressbar import ProgressBar


def populate_text_review_text_collections():
    db = DB()
    users = db.get_user_id_list()

    pb = ProgressBar(len(users)).start()
    for e, user in enumerate(users):
        pb.update(e+1)
        u_id = user['user_id']

        raw_text = db.get_user_review_text_stars_in(u_id, [1, 2])
        user_review_text = process_text(raw_text)

        obj = {'user_id': u_id,
               'text': user_review_text}
        db.yelp['user_review_text_12'].insert_one(obj)

    pb.finish()


def populate_user_review_text_collection():
    db = DB()
    users = db.get_user_id_list()

    pb = ProgressBar(len(users)).start()
    for e, user in enumerate(users):
        pb.update(e+1)
        u_id = user['user_id']

        raw_text = db.get_user_review_text_stars_in(u_id, [1, 2, 3, 4, 5])
        user_review_text = process_text(raw_text)

        obj = {'user_id': u_id,
               'text': user_review_text}
        db.yelp['user_review_text'].insert_one(obj)

    pb.finish()


def clean_text_review_collections():
    """ Clean the tables of empty values """
    db = DB()

    db.yelp['user_review_text_12'].remove({'text': {'$eq': ''}})
    db.yelp['user_review_text_45'].remove({'text': {'$eq': ''}})


if __name__ == '__main__':
    # populate_text_review_text_collections()
    # clean_text_review_collections()

    populate_user_review_text_collection()
