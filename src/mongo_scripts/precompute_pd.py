from database.dbio import DB
from lda.lda_letor_extractor import SimpleLDA
from progressbar import ProgressBar
import numpy as np


def pre_compute_user_prob_dists():
    db = DB()
    lda = SimpleLDA('/home/jaivalis/Desktop/LDA_user_item_joined')
    u = db.get_user_review_text_12_list()

    pb = ProgressBar(len(u)).start()
    for e, b in enumerate(u):
        pb.update(e+1)

        i_id = b['user_id']
        text = b['text']

        pd = lda.lda_res.get_text_prob_dist(text)
        if not np.all(pd == 0):
            db.set_w2v_vector('user_review_text_12', i_id, 'user_id',
                              'LDA_pd', pd)
    pb.finish()

    u = db.get_user_review_text_45_list()
    pb = ProgressBar(len(u)).start()
    for e, b in enumerate(u):
        pb.update(e+1)

        i_id = b['user_id']
        text = b['text']

        pd = lda.lda_res.get_text_prob_dist(text)

        if not np.all(pd == 0):
            db.set_w2v_vector('user_review_text_45', i_id, 'user_id',
                              'LDA_pd', pd)
    pb.finish()


def pre_compute_business_prob_dists():
    db = DB()
    lda = SimpleLDA('/home/jaivalis/Desktop/LDA_user_item_joined')
    l = db.get_business_review_text_12_list()

    pb = ProgressBar(len(l)).start()
    for e, b in enumerate(l):
        pb.update(e+1)

        i_id = b['item_id']
        text = b['text']

        pd = lda.lda_res.get_text_prob_dist(text)
        if not np.all(pd == 0):
            db.set_w2v_vector('business_review_text_12', i_id, 'item_id',
                              'LDA_pd', pd)
    pb.finish()

    l = db.get_business_review_text_45_list()
    pb = ProgressBar(len(l)).start()
    for e, b in enumerate(l):
        pb.update(e+1)

        i_id = b['item_id']
        text = b['text']

        pd = lda.lda_res.get_text_prob_dist(text)

        if not np.all(pd == 0):
            db.set_w2v_vector('business_review_text_45', i_id, 'item_id',
                              'LDA_pd', pd)
    pb.finish()

if __name__ == '__main__':
    # pre_compute_business_prob_dists()
    pre_compute_user_prob_dists()
