from collections import Counter
### https://www.jasondavies.com/wordcloud/#
CORPUS_FILE = "../output/serialized/corpus_per_item.data"
WORDCLOUD_FILE = "../output/wordcloud/word_cloud.txt"

dictionary = Counter()
with open(CORPUS_FILE, "r") as f:
    for line in f:
        dictionary.update(line.split())

N = 250
norm_factor = dictionary.most_common(N)[-1][1]
print norm_factor
with open(WORDCLOUD_FILE, 'w') as f:
    for i in dictionary.most_common(N):
        for reps in range(i[1] / norm_factor):
            f.write('{} '.format(i[0]))

