import math

if __name__ == '__main__':

    with open('../output/trust/lda_trust.txt', 'w') as trust_file, \
            open('../output/lda_user_user_similarities.csv', 'r') as input_csv:
        first = True
        for line in input_csv:
            if first:
                first = False
                continue
            parts = line.split(',')
            u1 = parts[0]
            u2 = parts[1]
            sim12 = float(parts[2])
            sim45 = float(parts[3])

            num = float('nan')
            if not math.isnan(sim12) and not math.isnan(sim45):
                num = (sim12 + sim45) / 2
            else:
                if math.isnan(sim12):
                    num = sim45
                    if math.isnan(num):
                        continue
                if math.isnan(sim45):
                    num = sim45
                    if math.isnan(num):
                        continue

            trust_file.write('{} {} {}\n'.format(u1, u2, num))
