import random
from collections import Counter

from progressbar import ProgressBar

from database.dbio import DB

PREDICTIONS_FOLDER = './output/predictions/'


def split_users(user_id_list, training_percentage=.8):
    """ Splits the users into training and test sets """
    training_size = int(training_percentage * len(user_id_list))

    random.shuffle(user_id_list)

    trainingSet = user_id_list[:training_size]
    testSet = user_id_list[training_size:]

    return trainingSet, testSet


def generate_training_file(trainingset, yelp):

    with open('data.out', 'w') as f:

        for uid in trainingset:

            for r in yelp.get_user_reviews(uid):
                item = r['business_id']
                stars = r['stars']

                f.write('{}\t{}\t{}\n'.format(uid, item, stars))


def write_pred_line(f, true_v, pred_v):
    f.write('{}\t{}\n'.format(true_v, pred_v))


def predict_average(testset, yelp):
    """
    """
    avg_stars_cache = {}
    out_path = PREDICTIONS_FOLDER + 'global_average.pred'

    with open(out_path, 'w') as f:
        pbar = ProgressBar(maxval=len(testset)).start()

        for e, u in enumerate(testset):
            pbar.update(e + 1)
            uid = u['user_id']

            for r in yelp.get_user_reviews(uid):
                b_id = r['business_id']
                true_v = r['stars']

                if b_id not in avg_stars_cache:
                    avg_stars_cache[b_id] = int(yelp.get_average_stars(b_id))
                pred_v = avg_stars_cache[b_id]

                write_pred_line(f, true_v, pred_v)
    pbar.finish()
    print 'output written to file {}'.format(out_path)


def predict_majority(db, training_set, test_set, output_path):
    """ This naive implementation predicts what the majority voted for. """
    maj_counters = {}
    maj_stars = {}

    for e, u in enumerate(training_set):
        u_id = u['user_id']
        for r in db.get_user_reviews(u_id):
            b_id = r['business_id']
            if b_id not in maj_counters:
                maj_counters[b_id] = Counter()
            maj_counters[b_id].update(str(r['stars']))

    # unpack majority counters
    for b_id in maj_counters:
        maj_stars[b_id] = maj_counters[b_id].most_common(1)[0][0]

    with open(output_path, 'w') as f:
        for u in test_set:
            u_id = u['user_id']
            for r in db.get_user_reviews(u_id):
                b_id = r['business_id']
                true_v = r['stars']
                pred_v = maj_stars[b_id] if b_id in maj_stars else random.randint(1, 5)

                write_pred_line(f, true_v, pred_v)


def predict_friends_average(testset, yelp, w1, w2, output_path):
    """ In this implementation the prediction for the score is given by the friends average rating. If such ratings
    don't exist, we fallback to the global average.
    :param testset:
    :param yelp:
    :param w1: Weight assigned to friend's average
    :param w2: Weight assigned to global average
    :return:
    """
    fallbacks = 0
    predictions = 0

    with open(output_path, 'w') as f:
        pbar = ProgressBar(maxval=len(testset)).start()

        for e, u_id in enumerate(testset):
            pbar.update(e + 1)

            friends_list = yelp.get_friends(u_id)
            review_list = []  # get rid of the slow pyMongo cursor
            for rr in yelp.get_user_reviews(u_id):
                review_list.append({
                    'business_id': rr['business_id'], 'stars': rr['stars']
                })

            for rr in review_list:
                predictions += 1
                b_id = rr['business_id']
                true_v = rr['stars']

                friends_avg = yelp.get_friends_average_stars(friends_list, b_id)
                global_avg = yelp.get_average_stars(b_id)

                if friends_avg is not None:  # linear combination
                    predicted_v = int(w1 * friends_avg + w2 * global_avg)
                else:  # fallback
                    fallbacks += 1
                    predicted_v = int(global_avg)

                write_pred_line(f, true_v, predicted_v)
    pbar.finish()


def weight_exhaustive_search(test_set, db):
    resolution = .01
    w1 = resolution

    while w1 <= 1.0:
        w2 = 1.0 - w1
        print '(w1, w2): ({}, {})'.format(w1, w2)

        predict_friends_average(test_set, db, w1, w2)
        w1 += resolution


def friends_average_pipeline(db, folds=5, training_percentage=.8):
    user_id_list = [u['user_id'] for u in db.user_list]
    print 'User list length: {}'.format(len(user_id_list))
    training_set, test_set = split_users(user_id_list, training_percentage=training_percentage)
    w1 = None
    w2 = None
    if w1 is None:
        weight_exhaustive_search(test_set, db)
    else:
        f = 0
        while f < folds:
            output_path = PREDICTIONS_FOLDER + 'friends_average({}-{}).pred'.format(w1, w2)
            training_set, test_set = split_users(user_id_list, training_percentage=training_percentage)
            predict_friends_average(test_set, db, w1, w2, output_path)
            f += 1


def majority_pipeline(db, folds=5, training_percentage=.8):
    f = 0
    user_id_list = db.get_user_id_list()
    # print 'User id list length: {}'.format(len(user_id_list))
    while f < folds:
        print 'Generating fold#{}...'.format(f+1),
        training_set, test_set = split_users(user_id_list, training_percentage=training_percentage)
        output_path = PREDICTIONS_FOLDER + 'majority{}fold/fold{}.pred'.format(folds, f+1)
        predict_majority(db, training_set, test_set, output_path)
        print '\t[DONE]'

        f += 1


if __name__ == '__main__':
    yelp = DB()

    # majority_pipeline(yelp, 1, 0)

    predict_average(yelp.get_user_id_list(), yelp)
    # # # TODO replace with optimal w1, w2
    # # w1 = None
    # # w2 = None
    # #
    # # METHOD = 'friends_average'
    # #
    # # if METHOD is 'global_average':
    # #
    # #     predict_average(testset, yelp)
    # #
    # # elif METHOD is 'all':
    # #     predict_average(testset, yelp)
    # #     predict_friends_average(testset, yelp, w1, w2)
