import re
from database.dbio import DB
from util.my_io import *

"""

All 'hacks' that should otherwise be done differently are here

"""


def sanitize_predictions_file(origin_file_path, target_file_path):
    """
    Replaces the [b_id, u_id] format in the predictions temp file to the standard pred file format
    :param origin_file_path: file to read predictions from
    :param target_file_path: file to write sanitized predictions to
    """
    yelp = DB()
    regex = '\[(.*?)\]'

    with open(origin_file_path, 'rb') as origin:
        with open(target_file_path, 'wb') as f:

            for line in origin:
                # line = codecs.decode(line, 'latin-1')
                line = str(line).encode('utf-8')
                tmp = re.search(regex, line).group(1).split(', ')

                b_id = tmp[0][2:-1]
                u_id = tmp[1][2:-1]

                true = int(yelp.get_true_label(u_id, b_id))
                try:
                    pred = int(line.split('\t')[-1])
                except ValueError:
                    continue
                write_prediction_line(f, true, pred)


if __name__ == '__main__':
    sanitize_predictions_file('../predictions', '../tf-idf.pred')