import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize

INPUT_FILE = '../output/lda_similarities.csv'


if __name__ == '__main__':
    df = pd.read_csv(INPUT_FILE,
                     dtype={'sim12': np.float64, 'sim45': np.float64, 'true_label': np.integer},
                     na_values=['nan'])

    print df
    plt.scatter(df.sim12, df.sim45, marker='o', edgecolor='b', facecolor='none', alpha=0.5)

    plt.show()

    x0 = 0.05
    fun = lambda x: 0.5 * np.exp(-x * (1-x))
    res = optimize.minimize(fun, x0, method='Nelder-Mead')

    """
    DONE
        (w1 * sim12 + w2 * sim45)
        generate user-user similarity and incorporate it with the social circles feature
        baseline: SoRec based on user-user similarity, replace NaN with 0.

        extract item-item similarity based on tags

    TODO:
        extract features to use for LTR

        user sensitive features: user's friends ratings of item
        user insensitive features:
            average rating of item,
            probability of the item to be in low rating ranking. Given item text, sim12, sim45
    """

    """
    Tomorrow 2.30
    Extract lda features for lemur, 1000 items 100 users

    Methods
        Proposed:
            Basic models:
                Friendship based
                LDA-L2R

            Modified models:
    """
