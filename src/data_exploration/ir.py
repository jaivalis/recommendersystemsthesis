import my_math
import pickle
import codecs
from progressbar import ProgressBar
from nltk import PorterStemmer
from nltk.tokenize import RegexpTokenizer

from util.my_io import *
from database.dbio import DB

porter = PorterStemmer()
tokenizer = RegexpTokenizer(r'\w+')


def tokenize(doc):
    """ Break a string into tokens, preserving URL tags as an entire token. """
    doc = codecs.decode(doc, "latin-1")
    return [porter.stem(w.lower()) for w in tokenizer.tokenize(doc) if w not in stopwords.words('english')]


class TfIdf(object):
    def __init__(self, corpus_filename, default_idf=1.5):
        """ Tf Idf model constructor
        :param corpus_filename: path to corpus file
        :param default_idf: default value to be returned for idf measure
        """
        self.doc_count = 0
        self.term_doc_count = {}
        self.default_idf = default_idf

        self.corpus_filename = corpus_filename
        self.stopwords = stopwords.words('english')

        try:
            self.documents = documents_to_dict('corpus_per_item.data')
        except IOError:
            print 'You first need to serialize the text [Run serialize_text.py for the files you need].'

        dictionary_filename = corpus_filename.split('.')[0] + '.p'
        if file_exists(dictionary_filename):  # parse dict file
            self.parse_dictionary(dictionary_filename)

        else:
            print 'Dictionary file \'{}\' not found, creating file now...\t'.format(dictionary_filename),
            self.generate_dictionary()
            print '[DONE]'
            print 'Saving file \'{}\''.format(dictionary_filename),
            self.save_dictionary_to_file(dictionary_filename)
            print '[DONE]'

    def parse_dictionary(self, dictionary_filename):
        """ Recover the idf dictionary to the specified file. """
        tmp = pickle.load(open(dictionary_filename, 'rb'))
        self.doc_count = int(tmp['self.doc_count'])
        tmp.pop('self.doc_count')
        self.term_doc_count = tmp

    def save_dictionary_to_file(self, idf_filename):
        """ Save the idf dictionary to the specified file. """
        self.term_doc_count['self.doc_count'] = self.doc_count
        pickle.dump(self.term_doc_count, open(idf_filename, 'wb'))
        self.term_doc_count.pop('self.doc_count')

    def add_input_document(self, text, doc_id):
        """ Add terms in the specified document to the idf dictionary. """
        self.doc_count += 1
        for token in tokenize(text):
            if token not in self.term_doc_count:  # token not seen before
                self.term_doc_count[token] = {doc_id: 1, '$document_frequency$': 0}
            else:  # token already exists
                if doc_id not in self.term_doc_count[token]:
                    self.term_doc_count[token][doc_id] = 1
                else:
                    self.term_doc_count[token][doc_id] += 1
            self.term_doc_count[token]['$document_frequency$'] += 1  # increment df for token in any case

    def generate_dictionary(self):
        """ Creates the inverse dictionary for the corpus file """
        with open(self.corpus_filename, 'r') as corpus:
            for line in corpus:

                tabs = line.split('\t')
                if len(tabs) > 1:
                    text = tabs[1]
                    doc_id = tabs[0]  # id of item/user

                self.add_input_document(text, doc_id)

    def get_doc_count(self):
        """Return the total number of documents in the IDF corpus."""
        return self.doc_count

    def get_idf(self, token):
        """Retrieve the IDF for the specified term.

           This is computed by taking the logarithm of (
           (number of documents in corpus) divided by (number of documents
            containing this term) ).
         """
        if token in self.stopwords:
            return 0.

        if token not in self.term_doc_count:  # unseen term this should never happen
            return self.default_idf

        return my_math.log(float(1. + self.get_doc_count()) / (1. + self.get_df(token)))

    def get_df(self, token):
        return self.term_doc_count[token]['$document_frequency$']

    def get_tf(self, token, doc_id):
        if token in self.term_doc_count and doc_id in self.term_doc_count[token]:
            return 1. + my_math.log(self.term_doc_count[token][doc_id])
        else:
            return .0

    def get_tf_idf(self, token, doc_id):
        return self.get_tf(token, doc_id) * self.get_idf(token)

    def _get_similarity(self, doc_id, query):
        ret = .0
        query_tokens = tokenize(query)

        for token in set(query_tokens):
            tf = self.get_tf(token, doc_id)
            idf = self.get_idf(token)
            ret += tf * idf
        return ret

    def get_similarities(self, doc_id_cursor, query):
        """
        Returns a dict containing the similarities given a query
        :param doc_id_cursor: the ids of items to look for
        :param query: the user text
        :return: a dict containing the similarities per item
        """
        ret = {}
        for entry in doc_id_cursor:
            doc_id = entry['business_id']
            ret[doc_id] = self._get_similarity(doc_id, query)
        return ret

    def get_query_tf_idf(self, query):
        ret = {}

        token_list = tokenize(query)
        for token in set(token_list):
            tf = float(token_list.count(token)) / len(token_list)
            idf = self.get_idf(token)

            ret[token] = tf * idf
        return ret

    def get_cosine_similarities(self, doc_id_cursor, query):
        ret = {}

        tf_idf_query = self.get_query_tf_idf(query)

        for doc in doc_id_cursor:
            doc_id = doc['business_id']

            dot_p = 0.
            q_vector = 0.
            d_vector = 0.

            for token in tokenize(query):
                dot_p += self.get_tf_idf(token, doc_id) * tf_idf_query[token]

                q_vector += tf_idf_query[token] ** 2
                d_vector += self.get_tf_idf(token, doc_id) ** 2

            cos = dot_p / my_math.sqrt(q_vector) * my_math.sqrt(d_vector)

            ret[doc_id] = cos

        return ret


if __name__ == '__main__':
    # model = TfIdf('corpus.data')
    model = TfIdf('corpus_per_item.data')
    # model = TfIdf('corpus_per_user.data')

    # parse docs to dictionary
    user_text_dict = documents_to_dict('corpus_per_user.data')
    yelp = DB()

    with open('tf-idf.pred', 'wb') as f:
        user_list = yelp.get_user_id_list()
        progress_bar = ProgressBar(maxval=len(user_list)).start()
        for e, u in enumerate(user_list):
            progress_bar.update(e + 1)

            u_id = u['user_id']
            user_text = user_text_dict[u_id]

            item_ids = yelp.get_user_reviewed_business_ids(u_id)

            similarities = model.get_cosine_similarities(item_ids, user_text)

            # normalize per user
            min_val = min(similarities.itervalues())
            max_val = max(similarities.itervalues())

            for sim in similarities:
                if min_val == max_val:
                    similarities[sim] = int(similarities[sim])
                else:  # generate a score in the range [1, 5]
                    similarities[sim] = 1 + int(4 * (similarities[sim] - min_val) / (max_val - min_val))

                # TODO change pred to be the actual rating instead of the business_id
                write_prediction_line(f, [sim, u_id], similarities[sim])

