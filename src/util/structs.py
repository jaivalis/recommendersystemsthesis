
class TwoWayDict(dict):
    def __setitem__(self, key, value):
        if key in self:
            del self[key]
        if value in self:
            del self[value]
        dict.__setitem__(self, key, value)
        dict.__setitem__(self, value, key)

    def __delitem__(self, key):
        dict.__delitem__(self, self[key])
        dict.__delitem__(self, key)

    def __len__(self):
        """Returns the number of connections"""
        return dict.__len__(self) // 2

class TrueLabels:
    def __init__(self, path='../output/ratings.txt'):
        self.tl = {}
        with open(path, 'r') as f:
            for line in f:
                parts = line.split()
                self.tl[parts[0] + '$' + parts[1]] = int(parts[2])

    def get_true_label(self, u, i):
        return self.tl[u + '$' + i]

