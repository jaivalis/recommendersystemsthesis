"""
Extracts a file called item_item_tags.txt that contains the item-item similarity based on the category tags of the db
"""
from database.dbio import DB
from progressbar import ProgressBar
from collections import Counter


def list_overlap(l1, l2):
    l1c = Counter(l1)
    l2c = Counter(l2)
    return len(l1c & l2c)

if __name__ == '__main__':
    db = DB()
    businesses = db.get_business_list()
    pb = ProgressBar(maxval=len(businesses)).start()

    with open('../output/item_item_tags.txt', 'w') as f:
        for e, b in enumerate(businesses):
            b_id = b['business_id']
            cats = b['categories']
            pb.update(e+1)

            comparable = db.get_business_in_categories(cats)

            for bb in comparable:
                common_cats = list_overlap(cats, bb['categories'])
                similarity = common_cats / float(len(cats))

                f.write('{},{},{}\n'.format(b_id, bb['business_id'], similarity))
    pb.finish()
