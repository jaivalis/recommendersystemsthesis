from database.dbio import DB
from data_exploration.serialize_text import process_text
from progressbar import *


# def populate_item_review_text_collection():
#     db = DB()
#     items = db.get_business_id_list()
#
#     pb = ProgressBar(len(items)).start()
#     for e, item in enumerate(items):
#         pb.update(e+1)
#         b_id = item['item_id']
#
#         raw_text = db.get_business_review_text_stars_in(b_id, [1, 2])
#         item_review_text = process_text(raw_text)
#
#         obj = {'item_id': b_id,
#                'text': item_review_text}
#         db.yelp['business_review_text_12'].insert_one(obj)
#
#     pb.finish()


def populate_item_review_text_collection():
    db = DB()
    items = db.get_business_id_list()

    bar = ProgressBar(len(items), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()
    for e, item in enumerate(items):
        bar.update(e+1)
        b_id = item['business_id']

        raw_text = db.get_business_review_text_stars_in(b_id, [1, 2, 3, 4, 5])
        item_review_text = process_text(raw_text)

        obj = {'item_id': b_id,
               'text': item_review_text}
        db.yelp['business_review_text'].insert_one(obj)

    bar.finish()


def clean_text_review_collections():
    """ Clean the tables of empty values """
    db = DB()
    db.yelp['business_review_text'].remove({'text': {'$eq': ''}})
    db.yelp['business_review_text_12'].remove({'text': {'$eq': ''}})
    db.yelp['business_review_text_45'].remove({'text': {'$eq': ''}})


if __name__ == '__main__':
    # populate_item_review_text_collection()
    clean_text_review_collections()
