from database.dbio import DB
from scipy.sparse import lil_matrix, coo_matrix
import numpy as np
import nimfa
try:
    import cPickle as pickle
except:
    import pickle


def dump_ratings_matrix(R, file_name):
    f = open(file_name, 'wb')
    try:
        pickle.dump(R, f)
    finally:
        f.close()


def read_ratings_matrix(file_name):
    f = open(file_name, 'rb')
    try:
        o = pickle.load(f)
        return o
    except EOFError:
        pass


def rmse(W, H, R):
    """
    Compute the RMSE error rate on MovieLens data set.

    :param W: Basis matrix of the fitted factorization model.
    :type W: `numpy.matrix`
    :param H: Mixture matrix of the fitted factorization model.
    :type H: `numpy.matrix`
    :param R: Ratings matrix.
    :type R: `numpy.matrix`
    """
    rmse = []
    coo = coo_matrix(R)
    for u, i, r in zip(coo.row, coo.col, coo.data):
        sc = max(min((W[u - 1, :] * H[:, i - 1])[0, 0], 5), 1)
        rmse.append((sc - r) ** 2)
    print("RMSE: %5.3f" % np.mean(rmse))

db = DB()

items_num_id = {}
users_num_id = {}
for e, i in enumerate(db.get_business_id_list()):
    items_num_id[str(i['business_id'])] = e
for e, i in enumerate(db.get_user_list()):
    users_num_id[str(i['user_id'])] = e

print len(items_num_id)
print len(users_num_id)

# r_matrix = lil_matrix((len(users_num_id), len(items_num_id)))
#
# err = 0
# for r in db.get_review_list():
#     try:
#         u_id = users_num_id[str(r['user_id'])]
#         b_id = items_num_id[str(r['business_id'])]
#         r_matrix[u_id, b_id] = int(r['stars'])
#     except KeyError:
#         err += 1
# dump_ratings_matrix(r_matrix, '/home/jaivalis/Desktop/r_matrix.dat')
# print 'wrote to file'
r_matrix = read_ratings_matrix('/home/jaivalis/Desktop/r_matrix.dat')
print 'read from file'

pmf = nimfa.Pmf(r_matrix, seed="random_vcol", rank=10, max_iter=12, rel_error=1e-5)
fit = pmf()
print("Algorithm: %s\nInitialization: %s\nRank: %d" % (pmf, pmf.seed, pmf.rank))
sparse_w, sparse_h = fit.fit.sparseness()
print("""Stats:
        - iterations: %d
        - Euclidean distance: %5.3f
        - Sparseness basis: %5.3f, mixture: %5.3f""" % (fit.fit.n_iter,
                                                        fit.distance(metric='euclidean'),
                                                        sparse_w, sparse_h))
W, H = fit.basis(), fit.coef()
rmse(W, H, r_matrix)
