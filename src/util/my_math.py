from scipy import linalg, dot
import numpy as np


def cosine_similarity(v1, v2):
    """ compute cosine similarity of v1 to v2: (v1 dot v1)/{||v1||*||v2||)
    :param v1: numpy array
    :param v2: numpy array
    :return: float
    """
    cos = dot(v1, v2.T)/linalg.norm(v1)/linalg.norm(v2)
    if not isinstance(cos, float):
        pass
    return cos


def weighted_average(v1, v2, (w1, w2)):
    assert w1 + w2 == 1.
    if v1 is not None and v2 is not None:  # mixture
        return (v1 * w1) + (v2 * w2)
    return v2 if v1 is None else v1  # one of the two


def matrix_column_mean(m, axis):
    if len(m.shape) == 1:  # length of text: 1 token
        return m
    return np.mean(m, axis=axis)
