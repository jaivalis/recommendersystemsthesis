from progressbar import *

from lda_result_parser import ResultFolderParser
from database.dbio import DB
from util.my_io import parse_relevant_pairs, parse_relevant_pairs_dense, format_letor_line
from util.structs import TrueLabels
from w2v.w2v import load_w2v_model, get_text_w2v_vector
from util.my_math import cosine_similarity, weighted_average

import numpy as np

OUTPUT_FOLDER = '../output/'


def get_user_item_lda_similarity_vector(u_id, b_id, db):
    """ Returns the user-item similarity based on the LDA vectors of the two.
    :param u_id: user id
    :param b_id: business id
    :param db: database
    :return: np array
    """
    u_12 = db.get_lda_pd_vector('user_review_text_12', u_id)
    u_45 = db.get_lda_pd_vector('user_review_text_45', u_id)
    b_12 = db.get_lda_pd_vector('business_review_text_12', b_id)
    b_45 = db.get_lda_pd_vector('business_review_text_45', b_id)
    if u_12 is None or u_45 is None or b_12 is None or b_45 is None:
        return None
    return np.concatenate((u_12*b_12, u_45*b_45))


class SimpleLDA:
    """ Doesn't take ratings into consideration, uses just one TandemResults instance. """
    def __init__(self, root_path):
        print 'Parsing LDA results folder...\t\t',
        self.lda_res = ResultFolderParser(root_path)
        self.true_labels = TrueLabels()
        print '[DONE]'

    def user_item_sim_generator(self, relevant_pairs, db):
        """ Yields user-item similarity used to output a LETOR line.
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :return: dict
        """
        pb = ProgressBar(len(relevant_pairs)).start()
        for e, user_id in enumerate(relevant_pairs):
            b_ids = relevant_pairs[user_id]
            pb.update(e+1)

            user_text = db.get_user_review_text(user_id)

            sim_dict = None
            if user_text is not None:
                sim_dict = self.lda_res.get_user_item_list_similarities(user_text, b_ids)

            if sim_dict is not None:
                for b_id in sim_dict:
                    yield {'user_id': user_id, 'business_id': b_id, 'similarity': sim_dict[b_id]}
        pb.finish()

    def letor_line_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of the LDA prob dist + the w2v vector
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        pb = ProgressBar(len(relevant_pairs)).start()

        for e, u in enumerate(relevant_pairs):
            pb.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                pd = self.lda_res.get_text_prob_dist(review_t)

                if not np.all(pd == 0):
                    w2v_vector = get_text_w2v_vector(w2v_model, review_t)

                    if w2v_vector is not None:
                        try:
                            v = np.concatenate((pd, w2v_vector))
                            not_skipped += 1
                            yield format_letor_line(self.true_labels.get_true_label(u, b), e, v)
                        except ValueError:
                            pass
        pb.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_w2v_similarity_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v text similarity
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                review_v = get_text_w2v_vector(w2v_model, review_t)

                if review_v is not None:
                    item_v_12 = db.get_w2v_vector('business_review_text_12', b, 'item_id', 50)
                    item_v_45 = db.get_w2v_vector('business_review_text_45', b, 'item_id', 50)

                    similarity = None
                    if item_v_12 is not None:
                        similarity = -cosine_similarity(review_v, item_v_12)
                    if item_v_45 is not None:
                        if similarity is None:
                            similarity = cosine_similarity(review_v, item_v_45)
                        else:
                            similarity += cosine_similarity(review_v, item_v_45)
                    if similarity is not None and isinstance(similarity, float):  # dirty fix
                        not_skipped += 1

                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, [similarity])
        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_w2v_social_similarity_only_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v similarity using friends's vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                review_v = get_text_w2v_vector(w2v_model, review_t)

                if review_v is not None:
                    item_v_12 = db.get_w2v_vector('business_review_text_12', b, 'item_id', 50)
                    item_v_45 = db.get_w2v_vector('business_review_text_45', b, 'item_id', 50)

                    social_v_12 = db.get_social_vector(u, [1, 2])
                    social_v_45 = db.get_social_vector(u, [4, 5])

                    review_v_12 = weighted_average(review_v, social_v_12, (.80, .20))
                    review_v_45 = weighted_average(review_v, social_v_45, (.80, .20))

                    similarity = None
                    if item_v_12 is not None:
                        similarity = -cosine_similarity(review_v_12, item_v_12)
                    if item_v_45 is not None:
                        if similarity is None:
                            similarity = cosine_similarity(review_v_45, item_v_45)
                        else:
                            similarity += cosine_similarity(review_v, item_v_45)
                    if similarity is not None and isinstance(similarity, float):  # dirty fix
                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, [similarity])
        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_w2v_social_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v similarity using friends's vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                review_v = get_text_w2v_vector(w2v_model, review_t)

                if review_v is not None:
                    item_v_12 = db.get_w2v_vector('business_review_text_12', b, 'item_id', 50)
                    item_v_45 = db.get_w2v_vector('business_review_text_45', b, 'item_id', 50)

                    social_v_12 = db.get_social_vector(u, [1, 2])
                    social_v_45 = db.get_social_vector(u, [4, 5])

                    review_v_12 = weighted_average(review_v, social_v_12, (.50, .50))
                    review_v_45 = weighted_average(review_v, social_v_45, (.50, .50))

                    similarity = None
                    if item_v_12 is not None:
                        similarity = -cosine_similarity(review_v_12, item_v_12)
                    if item_v_45 is not None:
                        if similarity is None:
                            similarity = cosine_similarity(review_v_45, item_v_45)
                        else:
                            similarity += cosine_similarity(review_v, item_v_45)
                    if similarity is not None and isinstance(similarity, float):  # dirty fix
                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, [similarity])
        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_w2v_social_vector_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v vectors using social vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                review_v = get_text_w2v_vector(w2v_model, review_t)

                if review_v is not None:
                    social_v_12 = db.get_social_vector(u, [1, 2])
                    social_v_45 = db.get_social_vector(u, [4, 5])

                    review_v_12 = weighted_average(review_v, social_v_12, (.50, .50))
                    review_v_45 = weighted_average(review_v, social_v_45, (.50, .50))

                    if review_v_12 is not None and review_v_45 is not None:
                        vector = np.concatenate((review_v_12, review_v_45))

                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, vector)

        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_lda_w2v_social_vector_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v vectors using social vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                review_v = get_text_w2v_vector(w2v_model, review_t)
                pd = self.lda_res.get_text_prob_dist(review_t)

                if review_v is not None and not np.all(pd == 0):
                    social_v_12 = db.get_social_vector(u, [1, 2])
                    social_v_45 = db.get_social_vector(u, [4, 5])

                    review_v_12 = weighted_average(review_v, social_v_12, (.50, .50))
                    review_v_45 = weighted_average(review_v, social_v_45, (.50, .50))

                    if review_v_12 is not None and review_v_45 is not None:
                        vector = np.concatenate((pd, review_v_12, review_v_45))

                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, vector)

        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def letor_w2v_social_vector_iu_prob_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v vectors using social vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)

                lda_sim_vec = get_user_item_lda_similarity_vector(u, b, db)
                review_v = get_text_w2v_vector(w2v_model, review_t)
                user_pd = self.lda_res.get_text_prob_dist(review_t)

                if review_v is not None and not np.all(user_pd == 0) and lda_sim_vec is not None:
                    social_v_12 = db.get_social_vector(u, [1, 2])
                    social_v_45 = db.get_social_vector(u, [4, 5])

                    review_v_12 = weighted_average(review_v, social_v_12, (.90, .10))
                    review_v_45 = weighted_average(review_v, social_v_45, (.90, .10))

                    if review_v_12 is not None and review_v_45 is not None:
                        vector = np.concatenate((user_pd, review_v_12, review_v_45, lda_sim_vec))

                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, vector)

        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    # 6
    def letor_social_vector_generator(self, relevant_pairs, db, w2v_model):
        """ Yields lines for the LETOR training file that consist of w2v vectors using social vectors
        :param relevant_pairs: dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :return:
        """
        processed, not_skipped = 0, 0
        bar = ProgressBar(len(relevant_pairs), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

        for e, u in enumerate(relevant_pairs):
            bar.update(e+1)

            for b in relevant_pairs[u]:
                processed += 1
                review_t = db.get_review_text(u, b)
                lda_vector = self.lda_res.get_text_prob_dist(review_t)

                if lda_vector is not None and not np.all(lda_vector == 0):
                    social_v = db.get_social_vector(u)

                    review_v = weighted_average(lda_vector, social_v, (.8, .2))

                    if review_v is not None:
                        vector = np.concatenate((lda_vector, review_v))

                        not_skipped += 1
                        yield format_letor_line(self.true_labels.get_true_label(u, b), e, vector)

        bar.finish()
        print '[zeroes, non-zero] [{}, {}].'.format(processed - not_skipped, not_skipped)

    def extract_letor_file(self, relevant_pairs, db, w2v_model, path, method):
        """ Extracts a LETOR training file
        dict: user_id{business_id_list}
        :param db: database
        :param w2v_model: word2vector model
        :param path: path of the file to be created
        :param method: type of file to extract
        """
        print 'Writing to file \'{}\'.'.format(path)

        methods = {0: self.letor_line_generator,
                   1: self.letor_w2v_similarity_generator,
                   2: self.letor_w2v_social_similarity_only_generator,
                   3: self.letor_w2v_social_vector_generator,
                   4: self.letor_lda_w2v_social_vector_generator,
                   5: self.letor_w2v_social_vector_iu_prob_generator,
                   6: self.letor_social_vector_generator}

        with open(path, 'w') as f:
            f.truncate()
            for line in methods[method](relevant_pairs, db, w2v_model):
                f.write(line + '\n')

    def extract_similarities(self, db, user_id):
        # item_doc = db.get_business_review_text(item_id)
        user_doc = db.get_user_review_text(user_id)

        # item_lda = self.lda_res.get_text_prob_dist(item_doc)
        user_lda = self.lda_res.get_text_prob_dist(user_doc)

        social_v = db.get_social_vector(user_id)
        hybrid_v = weighted_average(user_lda, social_v, (.8, .2))

        # sim_lda = cosine_similarity(item_lda, user_lda)
        # sim_hybrid = cosine_similarity(item_lda, hybrid_v)

        print 'a: {} b: {} c: {} d: {}'.format(user_lda[4],  user_lda[17], user_lda[30], user_lda[33])


def qualitative():
    user1 = 'JCfHBL4zGkPjtKVd9hS_Bw'
    user2 = 'wsouO79E-BW7lStwopkfjA'
    user3 = 'lkV_WCqCbpA5gUIHD6wtOw'
    user4 = 'K4jiuwpunwXd6vniQgQ4vg'
    db = DB()
    lda = SimpleLDA('/home/jaivalis/Desktop/LDA_user_item_joined')

    lda.extract_similarities(db, user1)
    lda.extract_similarities(db, user2)
    lda.extract_similarities(db, user3)
    lda.extract_similarities(db, user4)



def extract(method):
    db = DB()

    dense = True
    pre = OUTPUT_FOLDER + 'letor/'


    lda = SimpleLDA('/home/jaivalis/Desktop/LDA_user_item_joined')

    if not dense:
        rp = parse_relevant_pairs(OUTPUT_FOLDER + 'relevant_pairs.txt')
    else:
        rp = parse_relevant_pairs_dense(OUTPUT_FOLDER + 'relevant_pairs.txt')
        pre += 'dense_'
        print 'dense'

    w2v_model = load_w2v_model('../output/w2v_models/model_7000_50')

    file_names = {0: 'user_item.txt',
                  1: 'no_ratings_w2v@50.txt',
                  2: 'social[5050]_w2v@50_similarity_only.txt',
                  3: 'social[5050]_w2v_50@vector.txt',
                  4: 'lda_social[5050]_w2v@50_vector.txt',
                  5: 'w2v_social[9010]_iu_prob.txt',
                  6: 'sum_lda_social[8020].txt'}
    lda.extract_letor_file(rp, db, w2v_model, pre+file_names[method], method)




if __name__ == '__main__':
    # extract(method=6)
    # extract(method=4)
    # extract(method=3)
    # extract(method=2)

    qualitative()
