from database.dbio import DB
from progressbar import ProgressBar


OUTPUT_FOLDER = '../output/trust/'


def count_common_friends(u1, u2):
    """
    :param u1: user object
    :param u2: user object
    :return: integer
    """
    f1 = u1['friends']
    f2 = u2['friends']
    ret = len(list(set(f1) & set(f2)))  # return length of friends lists intersection
    return ret


def extract_trust_circle(db, u):
    """
    return a dict containing the user_ids of each of ones friends alongside the trust (Float) to them
    :param u: user object
    :return: dict
    """
    ret = {}
    for f_id in u['friends']:
        ret[f_id] = (1 + db.count_common_friends(u, db.get_user(f_id))) / float(len(u['friends']))
    return ret


def write_user_trust(f, u_id, trust_circle):
    """ Follows a format parseable by LibRec's SoRec implementation 'user_id user_id trust\n'
    :param f: file object
    :param u_id: user id
    :param trust_circle: dict containing the trust quantized
    """
    for f_id in trust_circle:
        f.write('{} {} {}\n'.format(u_id, f_id, trust_circle[f_id]))

if __name__ == '__main__':
    yelp = DB()
    users = yelp.get_user_list()

    with open(OUTPUT_FOLDER + 'trust.txt', 'w') as trust_file:
        progress_bar = ProgressBar(maxval=len(users)).start()
        for e, u in enumerate(users):
            progress_bar.update(e+1)
            circle = extract_trust_circle(yelp, u)

            write_user_trust(trust_file, u['user_id'], circle)
        progress_bar.finish()

