import math

"""
    Applies a simple linear regression model and calculates the errors
"""
def measure_model_mae(pred, tru):
    return math.fabs(tru - pred)

def measure_model_rmse(pred, tru):
    return (tru - pred) ** 2

def apply_model(sim12, sim45):
    avg = .0
    w_sim12 = -.076
    w_sim45 = -.138
    delta = 3.871
    return int(w_sim12 * sim12 + w_sim45 * sim45 + delta)

def get_averages():
    ret = [.0, .0]
    n = [.0, .0]
    first = True
    with open('../output/lda_user_item_similarities.csv', 'r') as f:
        for line in f:
            if first:
                first = False
                continue

            parts = line.split(',')
            sim_12 = float(parts[0])
            sim_45 = float(parts[1])
            if not math.isnan(sim_12):
                ret[0] += sim_12
                n[0] += 1
            if not math.isnan(sim_45):
                ret[1] += sim_45
                n[1] += 1
    return ret[0] / n[0], ret[1] / n[1]

if __name__ == '__main__':
    avg12, avg45 = get_averages()
    print avg12, avg45
    with open('../output/lda_user_item_similarities.csv', 'r') as f:
        first = True
        mae = .0
        rmse = .0
        N = .0
        for line in f:
            if first:
                first = False
                continue
            N += 1.
            parts = line.split(',')
            sim_12 = float(parts[0]) if not math.isnan(float(parts[0])) else avg12
            sim_45 = float(parts[1]) if not math.isnan(float(parts[1])) else avg45
            tl = float(parts[2])

            pred = apply_model(sim_12, sim_45)
            mae += measure_model_mae(tl, pred)
            rmse += measure_model_rmse(tl, pred)
        mae /= N
        rmse /= N
        rmse = math.sqrt(rmse)
        print 'RMSE:\t{}\nMAE:\t{}'.format(mae, rmse)



