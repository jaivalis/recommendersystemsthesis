from gensim.models import word2vec, Word2Vec

from database.dbio import DB
from util.my_math import cosine_similarity

import random
import nltk.data
import numpy as np

sentence_detector = nltk.data.load('tokenizers/punkt/english.pickle')


def load_w2v_model(path):
    return Word2Vec.load(path)


def get_w2v_similarity(model, t1, t2):
    """ Returns the word2vector based similarity of two pieces of text using cosine similarity on the two text vectors
    :param model: w2v model
    :param t1: str
    :param t2: str
    :return: float
    """
    if t1 is None or t2 is None:
        return None
    v1 = get_text_w2v_vector(model, t1)
    v2 = get_text_w2v_vector(model, t2)

    if v1 is not None and v2 is not None:
        return cosine_similarity(v1, v2)
    return None


def get_text_w2v_vector(model, text):
    """ Given a piece of text returns the vector representation given from the w2v model
    :param model: w2v model
    :param text: str
    :return: np vector
    """
    tmp = None
    for e, token in enumerate(text.split()):
        tv = get_token_vector(model, token)
        if tv is not None:
            if tmp is None:
                tmp = tv
            else:
                tmp = np.vstack([tmp, tv])
    if tmp is None or tmp.shape == ():
        return None
    if len(tmp.shape) == 1:  # length of text: 1 token
        return tmp
    ret = np.mean(tmp, axis=0)  # vertical
    return ret


def get_token_vector(model, token):
    """ Returns the raw numpy vector of a given token
    :param model: Word2Vec model
    :param token: str
    :return: np vector
    """
    if isinstance(token, unicode):
        token = str(token)
    if token in model:
        return model[token]
    return None


class Sentences:
    def __init__(self, review_count):
        db = DB()
        self.review_count = review_count
        self.reviews = db.get_user_review_text_list()

    def __iter__(self):
        random.shuffle(self.reviews)

        for e, review in enumerate(self.reviews):
            # ISSUE: returns one sentence per comment since the text is normalized and punctuation is already removed.
            for s in sentence_detector.tokenize(review['text'].strip()):
                yield s.split()

            if e == self.review_count-1:
                break

    def __len__(self):
        return len(self.reviews)


class Word2Vector:
    """
    word2vec implementation by Gensim
    For details, see: http://rare-technologies.com/word2vec-tutorial/
    """
    def __init__(self, training_size, layers):
        sentences = Sentences(training_size)

        mc = 10
        threads = 4
        print 'Training word2vec model... \n' \
              '\tParameters: {} reviews ({} available), {} least token appearances, {} NN layers, {} threads.'.\
            format(training_size, len(sentences), mc, layers, threads),
        self.model = word2vec.Word2Vec(sentences, min_count=mc, size=layers, workers=threads)
        print '\t [DONE]'

        path = '../output/w2v/model_{}_{}'.format(training_size, layers)
        self.model.save(path)
        print 'Model trained and saved in [{}]'.format(path)


if __name__ == '__main__':
    w2v = Word2Vector(7000, 50)
    # del w2v
    m = load_w2v_model('../output/w2v_models/model_7000_50')

    print get_token_vector(m, 'place')
