from database.dbio import DB

OUTPUT_FOLDER = '../output/'


if __name__ == '__main__':
    db = DB()
    distinct_users = db.get_distinct_user_ids()

    with open(OUTPUT_FOLDER + 'relevant_pairs.txt', 'w') as f:

        for u_id in distinct_users:
            relevant = db.get_user_reviewed_business_id_set(u_id)
            relevant = list(relevant)

            f.write('{}\t{}\n'.format(u_id, ','.join(relevant)))
