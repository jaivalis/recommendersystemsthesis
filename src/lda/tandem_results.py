from lda.lda_result_parser import ResultFolderParser


class TandemResults:
    def __init__(self, item_result_folder, user_result_folder):
        self.item_results = ResultFolderParser(item_result_folder)
        self.user_results = ResultFolderParser(user_result_folder)
        self.word_id_map = {}  # matches word ids from item results to user results

        for key in self.item_results.wordmap:
            if key in self.user_results.wordmap:
                # there are a small amount of words less in one result folder compared to the other, possibly 1
                # document missing from one of the lda data input files
                self.word_id_map[self.item_results.wordmap[key]] = self.user_results.wordmap[key]

    def __str__(self):
        return str(self.item_results) + str(self.user_results)

    def get_user_item_list_similarities(self, user_text, item_id_list):
        """ Returns the similarities of the user text to businesses in the given list
        :param user_text: str
        :param item_id_list: list<string>
        :return: dict<str><float>
        """
        if user_text:
            return self.item_results.get_similarities(user_text, item_id_list)
        return None

    def get_user_user_similarity(self, user_text, user_id):
        """ Returns the similarities of the user text to a given user in the given list
        :param user_text:
        :param user_id:
        :return:
        """
        if user_text:
            return self.user_results.get_similarities(user_text, [user_id])[user_id]
        return None

    def get_text_prob_dist(self, text):
        """ Returns the topic pd of a given text
        :param text: <str>
        :return: np.array
        """
        return self.item_results.get_text_prob_dist(text)

if __name__ == '__main__':
    p = TandemResults('../output/lda_res_per_rating/per_rating_item_21/LDASummary/data')
