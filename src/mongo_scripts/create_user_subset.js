db.createCollection('user_subset');
db.user.find({'$and': [
            {'$where': 'this.friends.length>2'},
            {'review_count': {'$gt': 2}}
        ]}).forEach(function(doc){
   db.user_subset.insert(doc);
});
db.user_subset.createIndex({ "user_id" : 1 });