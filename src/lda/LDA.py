import cPickle

from progressbar import ProgressBar
import numpy as np

from tandem_results import TandemResults
from database.dbio import DB
from util.my_io import parse_relevant_pairs, format_letor_line
from util.structs import TrueLabels

OUTPUT_FOLDER = '../output/'

LDA_PER_ITEM_21 = '../output/lda_res_per_rating/per_rating_item_21/LDASummary/data'
LDA_PER_USER_21 = '../output/lda_res_per_rating/per_rating_user_21/LDASummary/data'
LDA_PER_ITEM_45 = '../output/lda_res_per_rating/per_rating_item_45/LDASummary/data'
LDA_PER_USER_45 = '../output/lda_res_per_rating/per_rating_user_45/LDASummary/data'


def serialize_similarities(sim12, sim45, tl, u):
    if sim12 is None and sim45 is None:
        return None
    res = []

    no12 = True if sim12 is None else False
    no45 = True if sim45 is None else False

    keys = sim45.keys() if sim12 is None else sim12.keys()

    for key in keys:
        r12 = sim12[key] if not no12 else np.NaN
        r45 = sim45[key] if not no45 else np.NaN

        res.append(LDASimilarity(r12, r45, tl.get_true_label(u, key)))
    return res


def post_process_sim_dict(sim):
    """ Normalization to range [1, 5] and deletion of none predicted values
    :param sim: dict<str><float>
    :return: dict<str><int>
    """
    ret = {}
    factor = 1.0 / max(sim.itervalues())
    for k in sim:
        if sim[k] is not None:  # if is none exclude from ret
            ret[k] = int(sim[k] * factor * 5)
    return ret


class LDASimilarity:
    def __init__(self, sim12, sim45, true_label):
        sim12 = np.NaN if sim12 is None else sim12
        sim45 = np.NaN if sim45 is None else sim45

        self.sim12 = sim12
        self.sim45 = sim45
        self.true_label = true_label

    def __str__(self):  # returns the line in csv format
        return '{},{},{}\n'.format(self.sim12, self.sim45, self.true_label)

    @staticmethod
    def csv_format_line():
        return 'sim12,sim45,true_label' + '\n'


class LDA:
    """
    This class uses the rated comments to generate predictions
    """
    def __init__(self, serialize=False):
        self.low_scores = TandemResults(LDA_PER_ITEM_21, LDA_PER_USER_21)
        self.high_scores = TandemResults(LDA_PER_ITEM_45, LDA_PER_USER_45)
        self.true_labels = TrueLabels()
        if serialize:
            self.serialize()

    def __str__(self):
        return 'LDA Predictor:\n{}{}'.format(str(self.low_scores), str(self.high_scores))

    def serialize(self):
        with open('LDA_object.pickle', 'wb') as f:
            cPickle.dump(self, f)

    def user_item_sim_generator(self, relevant_pairs, db):
        """ Yields predictions given the relevant pairs
        :param relevant_pairs: dict <str><list>
        :return: dict<str><np.array>
        """
        unclassified = [0, 0]
        pb = ProgressBar(len(relevant_pairs)).start()
        for e, user_id in enumerate(relevant_pairs):
            b_ids = relevant_pairs[user_id]
            pb.update(e+1)

            user_text_12 = db.get_user_review_text_12(user_id)
            user_text_45 = db.get_user_review_text_45(user_id)

            sim_vector_12, sim_vector_45 = None, None
            if user_text_12 is not None:
                sim_vector_12 = self.low_scores.get_user_item_list_similarities(user_text_12, b_ids)
            if user_text_45 is not None:
                sim_vector_45 = self.high_scores.get_user_item_list_similarities(user_text_45, b_ids)

            if sim_vector_12 is None:
                unclassified[0] += 1
            if sim_vector_45 is None:
                unclassified[1] += 1  # cannot predict anything for this user don't yield anything

            res = serialize_similarities(sim_vector_12, sim_vector_45, self.true_labels, user_id)

            if res is not None:
                # res = post_process_sim_dict(res)
                # yield {'user_id': user_id, 'predictions': res}
                for r in res:
                    yield r
        pb.finish()
        print 'Missing review text in [low_ratings, high_ratings]: ' + str(unclassified) + ' reviews.'

    def user_user_sim_generator(self, relevant_pairs, db):
        pb = ProgressBar(len(relevant_pairs)).start()

        seen_pairs = set()
        for e, u1 in enumerate(relevant_pairs):
            pb.update(e+1)

            user_text_12 = db.get_user_review_text_12(u1)
            user_text_45 = db.get_user_review_text_45(u1)

            for u2 in relevant_pairs[u1]:
                if (u1, u2) not in seen_pairs and (u2, u1) not in seen_pairs:
                    sim12, sim45 = None, None

                    if user_text_12 is not None:
                        sim12 = self.low_scores.get_user_user_similarity(user_text_12, u2)
                    if user_text_45 is not None:
                        sim45 = self.high_scores.get_user_user_similarity(user_text_45, u2)

                    sim12 = np.NaN if sim12 is None else sim12
                    sim45 = np.NaN if sim45 is None else sim45

                    yield {'u1': u1, 'u2': u2, 'sim12': sim12, 'sim45': sim45}

                    seen_pairs.add((u1, u2))
        pb.finish()

    def letor_line_generator(self, relevant_pairs, db):
        pb = ProgressBar(len(relevant_pairs)).start()

        for e, u in enumerate(relevant_pairs):
            pb.update(e+1)

            user_text_12 = db.get_user_review_text_12(u)
            user_text_45 = db.get_user_review_text_45(u)

            if user_text_12 and user_text_45:
                v12 = self.low_scores.get_text_prob_dist(user_text_12)
                v45 = self.high_scores.get_text_prob_dist(user_text_12)

                for i in relevant_pairs[u]:
                    tl = self.true_labels.get_true_label(u, i)
                    yield format_letor_line(tl, u, np.concatenate((v12, v45)))
        pb.finish()

    def extract_user_item_similarities(self, db, relevant_pairs):
        with open(OUTPUT_FOLDER + 'lda_user_item_similarities.csv', 'w') as f:
            f.truncate()
            f.write(LDASimilarity.csv_format_line())
            for obj in self.user_item_sim_generator(relevant_pairs, db):
                f.write(str(obj))

    def extract_user_user_similarities(self, db, relevant_pairs):
        with open(OUTPUT_FOLDER + 'lda_user_user_similarities.csv', 'w') as f:
            f.truncate()
            f.write('u1,u2,sim12,sim45\n')
            for obj in self.user_user_sim_generator(relevant_pairs, db):
                f.write('{},{},{},{}\n'.format(obj['u1'], obj['u2'], obj['sim12'], obj['sim45']))

    def extract_user_item_letor_file(self, relevant_pairs, db, path):
        print 'Writing to file \'' + path + '\'.'
        with open(path, 'w') as f:
            f.truncate()

            for line in self.letor_line_generator(relevant_pairs, db):
                f.write(line + '\n')


def fetch_lda_obj():
    try:
        with open('LDA_object.pickle', 'r') as fp:
            print 'Found serialized LDA object, loading from file...',
            ret = cPickle.load(fp)
            print '\t[DONE]'
            return ret
    except IOError:
        print 'Could not retrieve serialized LDA object, constructing anew.'
        return LDA(serialize=True)


def extract_user_item_similarities(lda_obj):
    db = DB()
    rp = parse_relevant_pairs(OUTPUT_FOLDER + 'relevant_pairs.txt')
    lda_obj.extract_user_item_similarities(db, rp)


def extract_user_user_similarities(lda_obj):
    db = DB()
    rp = parse_relevant_pairs(OUTPUT_FOLDER + 'social_connections.txt')
    lda_obj.extract_user_user_similarities(db, rp)


def extract_user_item_letor_file(lda_obj):
    """ Extracts a file that consists of the LDA vectors as features to learn
    each user is a query.
    :param lda_obj:
    """
    db = DB()
    rp = parse_relevant_pairs(OUTPUT_FOLDER + 'relevant_pairs.txt')
    lda_obj.extract_user_item_letor(rp, db, OUTPUT_FOLDER + '/letor/letor_train.txt')


def lda_scaled_ratings():
    """ Uses the scaled ratings to extract similarity """
    lda = fetch_lda_obj()

    # extract_user_item_similarities(lda)
    # extract_user_user_similarities(lda)
    extract_user_item_letor_file(lda)

    # backup & cleanup
    lda.serialize()


if __name__ == '__main__':
    lda_scaled_ratings()
