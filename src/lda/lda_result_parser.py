import os
import numpy as np

from util.structs import TwoWayDict
from util.my_math import cosine_similarity


class ResultFolderParser:
    """ Loads an lda result folder to memory for faster result querying. """
    def __init__(self, root_folder_path):
        self.stats = {}
        self.p_word_given_topic = None
        self.word_assignments = None
        self.wordmap = TwoWayDict()
        self.document_id_map = TwoWayDict()  # dict[number] = doc_id

        for f in os.listdir(root_folder_path):
            file_path = root_folder_path + '/' + f
            if f.endswith('.others'):
                for line in open(file_path, 'r'):
                    parts = line.strip().split('=')
                    if parts[0] not in ['alpha', 'beta', 'liters']:
                        self.stats[parts[0]] = parts[1]
            elif f.endswith('.phi'):
                # This file contains the word-topic distributions, i.e., p(word|topic).
                # Each line is a topic, each column is a word in the vocabulary.
                self.p_word_given_topic = np.loadtxt(file_path)
            elif f.endswith('.tassign'):
                self._parse_tassign(file_path)
            elif f.endswith('.theta'):
                # This file contains the topic assignments for words in training data.
                # Each line is a document that consists of a list of <wordij>:<topic of wordij>
                pass
            elif f.endswith('.twords'):  # unimportant
                # This file contains twords most likely words of each topic. twords is specified in the command.
                # self.parse_twords(file_path)
                pass
            elif f.endswith('.map'):
                self._parse_document_id_map(file_path)
            elif f == 'wordmap.txt':
                self._parse_wordmap(file_path)

    def __str__(self):
        return 'LDA Data Stats:\t\t\t{}\t\tp(word|topic) matrix shape:\t\t{}\n'.format(
            str(self.stats), np.shape(self.p_word_given_topic)
        )

    def get_document_vector(self, doc_id):
        if doc_id not in self.document_id_map:  # no review text for the document
            return None
        index = int(self.document_id_map[doc_id])-1  # line number starts at 1 in file
        return self.p_word_given_topic[:, index]

    def get_similarities(self, text, item_id_list):
        """ Returns a dict containing the similarities per item_id
        :param text: user text
        :param item_id_list: list<str>
        :return: dict<str><float>
        """
        ret = {}

        user_vector = self._get_text_vector(text)
        for b_id in item_id_list:
            sim = None  # undefined value

            business_vector = self.get_document_vector(b_id)
            if business_vector is not None:
                sim = cosine_similarity(user_vector, business_vector)
            ret[b_id] = sim
        return ret

    def get_text_prob_dist(self, text):
        """ Returns the topic probability distribution given a string
        :param text: <str>
        :return: np.array
        """
        token_id_list = self._get_token_id_list(text)
        return self._get_text_vector(token_id_list)

    # private
    def _parse_tassign(self, f):
        """ This file contains the topic-document distributions, i.e., p(topic|document).
            Each line is a document and each column is a topic.
        :param f: file path
        :return:
        """
        with open(f, 'r') as fl:
            for line in fl:
                for pair in line.split():
                    parts = pair.split(':')
                    word_id = parts[0]
                    topic_id = parts[1]

    def _parse_document_id_map(self, f):
        with open(f, 'r') as fl:
            for line in fl:
                parts = line.split()
                self.document_id_map[parts[0]] = parts[1]  # dict[number] = doc_id

    def _parse_wordmap(self, f):
        """ This file contains the maps between words and word's IDs (integer).
        :param f: file path
        """
        with open(f, 'r') as fl:
            for line in fl:
                parts = line.split()
                if len(parts) == 2:
                    if not parts[0].isdigit():
                        self.wordmap[parts[0]] = parts[1]  # dict[word] = id

    def _get_token_id_list(self, text):
        """ Given the user text, returns a list with all the tokens' ids
        :param text: <str>
        :return: list<int>
        """
        ret = []
        text = text.encode("utf-8")
        for token in text.split():
            if not token.isdigit() and token in self.wordmap:
                ret.append(int(self.wordmap[token]))
        return ret

    def _get_text_vector(self, token_id_list):
        """ Returns the probability distribution for a given user text
        :param token_id_list: list<int>
        :return: np.array
        """
        ret = np.zeros([50])  # 50 is the number of topics
        if token_id_list:  # safe div
            for token in token_id_list:
                ret += self.p_word_given_topic[:, token]  # vectors of size 50 again
            div = len(token_id_list)
            ret /= div  # ensuring the prob dists are in range [0, 1]
        return ret
