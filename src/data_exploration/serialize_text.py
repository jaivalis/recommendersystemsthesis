import nltk
import codecs
from progressbar import *
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from database.dbio import DB
import nltk.data

import threading

CORPUS_FILE = 'corpus.data'
OUTPUT_FOLDER = '../output/serialized/'
CORPUS_PER_USER_FILE = OUTPUT_FOLDER + 'corpus_per_user.data'
CORPUS_PER_ITEM_FILE = OUTPUT_FOLDER + 'corpus_per_item.data'
CORPUS_PER_SENTENCE_FILE = OUTPUT_FOLDER + 'corpus_per_sentence.data'

porter = nltk.PorterStemmer()
tokenizer = RegexpTokenizer(r'\w+')
sentence_detector = nltk.data.load('tokenizers/punkt/english.pickle')


def process_text(doc):
    """ Pre-process each document and return a string. """
    ret = []
    for w in tokenizer.tokenize(doc):
        w = w.lower()
        if w not in stopwords.words('english'):
            ret.append(porter.stem(w))
    return ' '.join(ret)


def serialize_corpus():
    yelp = DB()
    reviews = yelp.get_review_text_list()

    with codecs.open(OUTPUT_FOLDER + CORPUS_FILE, 'w', 'utf-8') as f:
        for e, r in enumerate(reviews):
            text = process_text(codecs.decode(r['text'], "latin-1"))
            f.write(text + '\n')


def serialize_corpus_per_sentence():
    """ Serialize reviews per sentence, one sentence per line
    :return:
    """
    yelp = DB()
    reviews = yelp.get_review_list()

    with codecs.open(CORPUS_PER_SENTENCE_FILE, 'w', 'utf-8') as f:
        pbar = ProgressBar(len(list(reviews))).start()
        for e, r in enumerate(reviews):
            pbar.update(e + 1)
            text = process_text(codecs.decode(r['text'], "latin-1"))
            for sentence in sentence_detector.tokenize(text.strip()):
                f.write(sentence + '\n')
        pbar.finish()


def serialize_per_item():
    yelp = DB()
    print 'Serializing per item'

    with codecs.open(CORPUS_PER_ITEM_FILE, 'w', 'utf-8') as f:
        business_ids = yelp.get_distinct_business_ids()

        pbar = ProgressBar(len(list(business_ids))).start()
        for e, b_id in enumerate(business_ids):  # one line per-item
            pbar.update(e + 1)

            f.write(b_id + '\t')
            for r in yelp.get_business_reviews(b_id):
                text = process_text(codecs.decode(r['text'], "latin-1"))
                f.write(text + ' ')
            f.write('\n')
        pbar.finish()


def serialize_per_user():
    yelp = DB()
    print 'Serializing per user'

    with codecs.open(CORPUS_PER_USER_FILE, 'w', 'utf-8') as f:
        user_ids = list(yelp.get_distinct_user_ids())
        pbar = ProgressBar(len(user_ids)).start()

        for e, u_id in enumerate(user_ids):  # one line per-user
            pbar.update(e+1)

            f.write(u_id + '\t')
            for r in yelp.get_user_reviews(u_id):
                text = process_text(r['text'])
                f.write(text + ' ')
            f.write('\n')
        pbar.finish()


def _get_user_reviews_string_per_stars(yelp, u_id, stars):
    ret = u_id + '\t'
    for r in yelp.get_user_reviews_stars_in(u_id, stars):
        text = process_text(r['text'])
        ret += text + ' '

    if ret == u_id + '\t':  # no such reviews
        return None
    return ret + '\n'


def get_user_review_text(yelp, u_id):
    t = yelp.get_user_review_text(u_id)
    if t is None:
        return None
    return u_id + '\t' + t + '\n'


def _get_business_reviews_string_per_stars(yelp, b_id, stars):
    ret = b_id + '\t'
    for r in yelp.get_business_reviews_stars_in(b_id, stars):
        text = process_text(r['text'])
        ret += text + ' '

    if ret == b_id + '\t':  # no such reviews
        return None
    return ret + '\n'


def serialize_per_user_rating():
    yelp = DB()
    print 'Serializing per user rating'

    with codecs.open(OUTPUT_FOLDER + 'per_rating_user_45.data', 'w', 'utf-8') as high_ratings_f, \
            codecs.open(OUTPUT_FOLDER + 'per_rating_user_3.data', 'w', 'utf-8') as neutral_ratings_f, \
            codecs.open(OUTPUT_FOLDER + 'per_rating_user_21.data', 'w', 'utf-8') as low_ratings_f:

            user_ids = list(yelp.get_distinct_user_ids())
            pbar = ProgressBar(len(user_ids)).start()

            for e, u_id in enumerate(user_ids):  # one line per-user
                pbar.update(e+1)

                high_reviews = _get_user_reviews_string_per_stars(yelp, u_id, [4, 5])
                if high_reviews is not None:
                    high_ratings_f.write(high_reviews)

                neutral_reviews = _get_user_reviews_string_per_stars(yelp, u_id, [3])
                if neutral_reviews is not None:
                    neutral_ratings_f.write(neutral_reviews)

                low_reviews = _get_user_reviews_string_per_stars(yelp, u_id, [1, 2])
                if low_reviews is not None:
                    low_ratings_f.write(low_reviews)

            pbar.finish()


def serialize_per_item_rating():
    yelp = DB()
    print 'Serializing per item rating'

    with codecs.open(OUTPUT_FOLDER + 'per_rating_item_45.data', 'w', 'utf-8') as high_ratings_f, \
            codecs.open(OUTPUT_FOLDER + 'per_rating_item_3.data', 'w', 'utf-8') as neutral_ratings_f, \
            codecs.open(OUTPUT_FOLDER + 'per_rating_item_21.data', 'w', 'utf-8') as low_ratings_f:

            business_ids = list(yelp.get_distinct_business_ids())
            pbar = ProgressBar(len(business_ids)).start()

            for e, b_id in enumerate(business_ids):  # one line per-user
                pbar.update(e+1)

                high_reviews = _get_business_reviews_string_per_stars(yelp, b_id, [4, 5])
                if high_reviews is not None:
                    high_ratings_f.write(high_reviews)

                neutral_reviews = _get_business_reviews_string_per_stars(yelp, b_id, [3])
                if neutral_reviews is not None:
                    neutral_ratings_f.write(neutral_reviews)

                low_reviews = _get_business_reviews_string_per_stars(yelp, b_id, [1, 2])
                if low_reviews is not None:
                    low_ratings_f.write(low_reviews)

            pbar.finish()


def threaded_serialize_per_user_and_per_item():
    t1 = threading.Thread(target=serialize_per_user)
    t2 = threading.Thread(target=serialize_per_item)

    t1.start()
    t2.start()

    t1.join()
    t2.join()


def threaded_serialize_per_user_and_per_item_rating():
    t1 = threading.Thread(target=serialize_per_user_rating)
    t2 = threading.Thread(target=serialize_per_item_rating)

    t1.start()
    t2.start()

    t1.join()
    t2.join()


def serialize_unified():
    """ Creates a file that consists of user-corpus lines followed by item-corpus lines. """
    yelp = DB()
    print 'Serializing unified...',
    with codecs.open(OUTPUT_FOLDER + 'unified.data', 'w', 'utf-8') as f:

            business_ids = list(yelp.get_distinct_business_ids())
            user_ids = list(yelp.get_distinct_user_ids())
            bar = ProgressBar(len(business_ids), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()

            for e, b_id in enumerate(business_ids):  # one line item
                bar.update(e+1)

                rev = _get_business_reviews_string_per_stars(yelp, b_id, [1, 2, 3, 4, 5])
                if rev is not None:
                    f.write(rev)
            bar.finish()

            bar = ProgressBar(len(user_ids), [RotatingMarker(), ' ', Percentage(), ' ', Bar(), ' ', ETA()]).start()
            for e, u_id in enumerate(business_ids):  # one line per-user
                bar.update(e+1)

                rev = get_user_review_text(yelp, u_id)
                if rev is not None:
                    f.write(rev)
            bar.finish()
    print '[DONE]'


if __name__ == '__main__':
    # older main :
    # serialize_corpus()
    # old main :
    # threaded_serialize_per_user_and_per_item()
    # serialize_corpus_per_sentence()
    serialize_unified()
