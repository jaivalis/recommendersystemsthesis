from scipy.sparse import coo_matrix
import numpy as np
import nimfa
try:
    import cPickle as pickle
except:
    import pickle


def read_ratings_matrix(file_name):
    f = open(file_name, 'rb')
    try:
        o = pickle.load(f)
        return o
    except EOFError:
        pass


def rmse(W, H, R):
    """
    Compute the RMSE error rate on MovieLens data set.

    :param W: Basis matrix of the fitted factorization model.
    :type W: `numpy.matrix`
    :param H: Mixture matrix of the fitted factorization model.
    :type H: `numpy.matrix`
    :param R: Ratings matrix.
    :type R: `numpy.matrix`
    """
    rmse = []
    coo = coo_matrix(R)
    for u, i, r in zip(coo.row, coo.col, coo.data):
        sc = max(min((W[u - 1, :] * H[:, i - 1])[0, 0], 5), 1)
        rmse.append((sc - r) ** 2)
    print("RMSE: %5.3f" % np.mean(rmse))

def mae(W, H, R):
    """
    Compute the MAE error rate on MovieLens data set.

    :param W: Basis matrix of the fitted factorization model.
    :type W: `numpy.matrix`
    :param H: Mixture matrix of the fitted factorization model.
    :type H: `numpy.matrix`
    :param R: Ratings matrix.
    :type R: `numpy.matrix`
    """
    rmse = []
    coo = coo_matrix(R)
    for u, i, r in zip(coo.row, coo.col, coo.data):
        sc = max(min((W[u - 1, :] * H[:, i - 1])[0, 0], 5), 1)
        rmse.append((sc - r) ** 2)
    print("RMSE: %5.3f" % np.mean(rmse))

r_matrix = read_ratings_matrix('r_matrix.dat')
print 'successfully read matrix from file'

pmf = nimfa.Pmf(r_matrix, seed="random_vcol", rank=10, max_iter=12, rel_error=1e-5)
fit = pmf()
print("Algorithm: %s\nInitialization: %s\nRank: %d" % (pmf, pmf.seed, pmf.rank))
sparse_w, sparse_h = fit.fit.sparseness()
print("""Stats:
        - iterations: %d
        - Euclidean distance: %5.3f
        - Sparseness basis: %5.3f, mixture: %5.3f""" % (fit.fit.n_iter,
                                                        fit.distance(metric='euclidean'),
                                                        sparse_w, sparse_h))
W, H = fit.basis(), fit.coef()
rmse(W, H, r_matrix)
