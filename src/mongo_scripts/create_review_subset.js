db.createCollection('review_subset');
db.review.find({'user_id': 
        {'$in': 
            db.user_subset.distinct('user_id')
        }}).forEach(function(doc){
   db.review_subset.insert(doc);
});
db.review_subset.createIndex({ "user_id" : 1 });
db.review_subset.createIndex({ "business_id" : 1 });
db.review_subset.createIndex({ "user_id": 1, "business_id" : 1 });