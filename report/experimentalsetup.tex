%!TEX root = ./main2.tex
% Gives all the implementation details people need to know in order to reproduce the experiments.
\chapter{Experimental Setup}
\label{ch:experimental_setup}
	In this Chapter we first introduce the dataset we use through some interesting statistics.
	Next, we go through each specific method we implement and the specifics of our implementation for it.
	Finally, we present the evaluation metrics that we use to evaluate our methods.

	\section{Dataset selection}
	\label{sec:dataset}
	We saw in Chapter \ref{ch:related_work} that a very common application for RS is on datasets that include user ratings of items.
	In this work, motivated by our two research questions we lay out some requirements that need to be met to enable us to answer those questions.
	Namely we require the dataset to contain two features, first, a social feature which somewhat reflects users' social circles and second, we want to feedback in the form of free text that accompanies the rating.

	A variety of datasets are employed in the bibliography\footnote{Movielens, Twitter etc.}\cite{Miller03movielensunplugged,DBLP:conf/ijcai/AbelGHT13,AAAIW125262,Sarwar00applicationof}, however one that meets our needs and the one we used is the Yelp dataset \citep{yelp}.
	This dataset has been provided by a commercial website therefore includes a lot of information that is irrelevant to our work, however it satisfies our needs for social and textual information.

	\section{Yelp academic dataset}
	Yelp is a company which provides a business reviewing platform.
	Users can rate and comment on services provided by businesses.
	Furthermore, this service provides the users with the ability to incorporate a social aspect to their profiles by adding people as friends, therefore following their activity on the platform.
	Businesses in turn are provided with a platform which encourages their clientele to leave constructive feedback, therefore enabling them to improve on aspects that their clients mention in their reviews.

	The Yelp academic dataset\footnote{\url{https://www.yelp.com/dataset_challenge/dataset}} consists of five different types of information, namely: ``business'', ``review'', ``user'', ``check-in'', ``tip''.
	Table \ref{tbl:yelp_dataset} provides a brief overview.

	\begin{table*}[!t]
	\centering\small
	\begin{tabular}{@{}l@{~~}p{6.2cm}@{~~~}c@{}}
	\toprule
	Table 			  & Description																& Entry count \\
	\midrule
	\textit{business} & \raggedright Object to the recommendation process (\textit{item}, $i$)  & $15,584$\\
	\textit{user}     & \raggedright Subject to the recommendation process (\textit{user}, $u$) & $70,816$\\
	\textit{review}   & \raggedright User generated feedback (\textit{rating}, $r_{u,i}$) 		& $335,021$\\
	\midrule
	\textit{check-in} & \raggedright Holds the count of users that visited a business on an hourly basis 		& $11,433$\\
	\textit{tip}      & \raggedright A short message left by a user regarding a business for future reference   & $113,992$\\
	\bottomrule
	\end{tabular}
	\caption[Overview of Yelp academic dataset]{An overview of the Yelp academic dataset}
	\label{tbl:yelp_dataset}
	\end{table*}

	The \textit{business} data type refers to what we have been referring to as an \textit{item} throughout this document.
	The information that is enclosed in this array consists of general information about the business such as ``type'', ``id'', ``address'', ``review count'', ``stars''\footnote{As in other similar platforms the rating of a business is evaluated with a star pointing system which varies in the range $[1, 5]$} (rounded to $0.5$) and ``opening hours''.
	This table contains $15,584$ entries.

	The \textit{user} table contains user credentials (``user name''), ``review count'', ``average stars'' and ``member since''.
	This table also contains information about the given user's social circle, the field ``friends'' maps all of the users friend ids, therefore enabling us to construct a social network graph with user-to-user relations.
	The ``user'' table consists of $70816$ user instances.

	Lastly the \textit{review} table is the most important resource used in our implementation since the recommendations we produce are largely based on the feedback that the user has left to other items.
	Each entry of this table contains information about a single review of a user to an item, more specifically the information contained is as follows: ``business id'', ``user id'', ``stars'' (an integer in the range $[1, 5]$), ``text'', ``date'' and ``votes''.
	In the dataset we have $335,021$ review entries.

	To reduce noise we opted out users who have not left at least two reviews, considering that the main goal of this work is not to address the \textit{cold-start problem}.
	This way we make sure that the users we are left with have a consistent flow of reviews and we therefore, we can proceed to make recommendations to them based on their previous activity on the platform.
	In addition, since we want to incorporate social circles in our recommendations we selected users who have at least two friends in their circles.
	These actions left us with a subset database consisting of a total of $14,311$ businesses in ``business\_subset'', $14,352$ users in ``user\_subset'' and finally $193,995$ reviews in ``review\_subset'' to work with.

	In their work \citet{Adomavicius_stabilityof} confirm the correlation between data density and recommendation accuracy.
	Having introduced the the dataset, we can now define the density of the user-item matrix as such:

	\begin{equation}
		\frac{193995}{14352 \times 14311} = 0.094451341\%
	\end{equation}

	To put this into perspective, compared to some examples of similarly popular collaborative filtering datasets in the research community, Movielens 100K\footnote{\url{https://www.cs.umn.edu/Research/GroupLens}} ($100,000$ ratings, $1,682$ items and $1682$ ratings) and Netflix\citep{Bennett07thenetflix} ($3,000$ users $3,000$ items and $105,256$ ratings) are $6.30\%$ and $1.17\%$ respectively\citep{Adomavicius_stabilityof}.
	In particular in the Movielens dataset all the users are guaranteed to have votes on at least 20 items whereas in our case we are using 2 as a threshold.
	It is now safe to say that compared to others, our user-item matrix is a rather sparse one.
	In yelp dataset however we are given the opportunity to exploit social network structures, which are not available in the two previously mentioned examples.
	The statistics regarding the yelp user-item ratings matrix are summarized in Table \ref{tbl:user_item_rating_matrix_stats}.

	\begin{table}[ht!]
		\centering\small
		\begin{tabular}{c c c}
		\toprule
		Statistics 				& User 		& Item \\
		\midrule
		Min. number of ratings	& $3$		& $3$ \\
		Max. number of ratings	& $3,286$	& $1,170$ \\
		\midrule
		Avg. number of ratings 	& $107$		& $24$ \\
		\bottomrule
		\end{tabular}
		\caption[User-Item Rating Matrix statistics]{Statistics of the rating matrix}
		\label{tbl:user_item_rating_matrix_stats}
	\end{table}


	\subsection{Review Text}
		In this section we will discuss the ``text'' field of the ``review\_subset'' table.
		This field is deemed the most important part of the review itself, since the main assumption behind our methods is that in the text review, the taste of the user is expressed.
		Therefore deciphering what the comment consists of will ideally boost the performance of a method that utilizes only the user rating.

	    \subsubsection{Text preprocessing}
	        Prior to using the review text, some text preprocessing needs to be applied.
	        This preprocessing consists of three steps as per traditional Information Retrieval, namely: text normalization, stop-word removal and stemming.

	        The goal of the normalization step is to map terms in the text into some universal form.
	        For example we would like the tokens in capital letters to refer to the same tokens in lowercase.

	        Stop-words are those words whose appearance in text is so frequent that their contribution in selecting and matching documents is minimal.
	        Some examples of those words are given: \textit{a}, \textit{and}, \textit{at}, \textit{by} etc.
	        All such words are therefore removed from the user comments.

	        Finally during stemming, words are converted in a heuristic process that aims for their lemmatization.
	        Lemmatization is a linguistic, language specific process which implies reduction of tokens to their dictionary headword form, otherwise called the lemma of the word.
	        For example the words \textit{am}, \textit{are}, \textit{is} following the linguistic rules would all reduce to \textit{be}.
	        In our implementation we made use of the Porter Stemmer algorithm implementation from the NLTK package \cite{BirdKleinLoper09}.

		\begin{figure}[ht]
		\centering
		\includegraphics[width=\textwidth]{img/wordcloud.png}
		\caption[Word cloud]{Word cloud generated from text in user reviews}
		\label{fig:wordcloud}
		\end{figure}

		In Figure \ref{fig:wordcloud} we observe the $250$ most commonly used words among the user comments (after the preprocessing steps, hence some stripped of their suffixes).
		Sizes of words correspond to the frequency of their appearances.

		Other interesting statistics of the review text data include, the average token count per review, the average sentence length of each review.
		These statistics can be found in table \ref{tbl:review_text_stats}.

		\begin{table}[ht]
		\centering\small
		\begin{tabular}{l r}
		\toprule
		Statistics Type 					& Value\\
		\midrule
		average word count per review		& $150$ \\
		average sentence count per review	& $8$ \\
		\bottomrule
		\end{tabular}
		\caption[Review text statistics]{Statistics regarding the review text}
		\label{tbl:review_text_stats}
		\end{table}

		Finally, Figure \ref{fig:review_length_per_stars} depicts the average comment length of the comment (in tokens) per rating.
		We can see that users tend to be more verbose on average when they are dissatisfied by the services provided.
		On the contrary a high rating seems to be an adequate indicator of satisfaction, thus resulting in less lengthy item reviews.

		\begin{figure}[ht]
		\centering
		\includegraphics[width=\textwidth]{img/review_length_per_stars.png}
		\caption[Review length per review score]{Average length of review per score.}
		\label{fig:review_length_per_stars}
		\end{figure}

\section{Baseline Methods and Comparisons}
\label{sec:baselines_and_comparisons}
	In this section we discuss the methods we used as baselines for the recommendation task as well as the methods implemented.
	Each subsection presents a more thorough view on each specific method.

	In Table \ref{tbl:baselinesandmethods} we present an overview of what lies in this section.
	All the methods implemented and tested on our dataset that will be discussed as well as the abbreviation with which they will be referenced in the rest of the paper are presented.

	There exist a plethora of RSs that base their predictions strictly on the ratings matrix.
    For the purposes of this experiment we pick a handful of state-of-the-art methods and compare them against our methods.
    The algorithms we picked are \textit{Probabilistic Matrix Factorization (PMF)}, \textit{Bayesian PMF}, \textit{Social Recommendation (SoRec)}\footnote{\textit{SoRec} which introduces a trust based social recommendation. For the purpose of this paper we alternate between different variants for the user-to-user trust measure and keep the rest of the implementation intact as provided by \cite{Ma2008SoRecSR}}, \textit{BiasedMF} and \textit{Regularized Singular Value Decomposition (RegSVD)}.
    We have introduced those methods in Section \ref{sec:approaches}
    % why do I pick those? why they agree with what I do, why I ruled out if I ruled out any (external knowledge etc.)
    % ~1 paragraph
    % one subsection per method.
    % use my terminology, not each paper's

	\begin{table*}[!t]
	\centering\small
	\begin{tabular}{@{}l@{~~}p{6.2cm}@{~~~}c@{}}
	\toprule
	\textbf{Acronym} & \textbf{Gloss} & \textbf{Reference} \\
	\midrule
	\multicolumn{3}{@{}l}{\emph{Naive Methods}}\\
	  Global Average	& \raggedright Proof of concept naive method & - \\
	  Majority		& \raggedright Proof of concept naive method & - \\
	\midrule
	\multicolumn{3}{@{}l}{\emph{Baselines}}\\
	  Friends Average	& \raggedright Extension of Global Average towards personalization & This thesis \\
	  PMF				& \raggedright Probabilistic Matrix Factorization & \citep{Salakhutdinov_probabilisticmatrix} \\
	  Bayesian PMF		& \raggedright Bayesian probabilistic matrix factorization using Markov Chain Monte Carlo & \citep{Salakhutdinov08bayesianprobabilistic} \\
	  BiasedMF		& \raggedright Implicit Recommender Systems: Biased Matrix Factorization & \citep{Koren:2008:FMN:1401890.1401944} \\
	  SoRec			& \raggedright Social recommendation using probabilistic matrix factorization & \citep{Ma08sorec:social} \\
	  RegSVD		& \raggedright Regularized Singular Value Decomposition  & \citep{7581652} \\
	  LambdaMART	& \raggedright Adapting Boosting for Information Retrieval Measures  & \citep{Wu09learningto} \\
	\midrule
	\multicolumn{3}{@{}l}{\emph{Our Contribution}}\\
	  Text-enhanced Recommendation		& \raggedright Leverages the textual information & This thesis \\
	  Social Enhanced Recommendation	& \raggedright Leverages the social information & This thesis \\
	  Text-Social hybrid Recommendation	& \raggedright Hybrid method that leverages both textual and social features & This thesis \\
	\bottomrule
	\end{tabular}
	\caption[All method overview]{An overview of the methods implemented and used in this paper.}
	\label{tbl:baselinesandmethods}
	\end{table*}

	\subsection{Global Average}
		The most intuitive and simplistic way to generate a recommendation is by using the average ratings of items.
		This method obviously does not incorporate any type of personalization in the results since the recommendations will be the same for any given user.
		For any given user $u$, the predicted rating for an item $i$ is given by:
		\begin{equation}
			r_{u,i} = mean(r_i)
			\label{eq:global_average}
		\end{equation}

		Where $mean(r_i)$ stands for the average star rating of the item $i$ throughout all reviews concerning this item.
		The predicted rating for a user-item pair produced consists solely of the overall average rating for that item, without making any assumption about the user.

	\subsection{Majority}
		For proof of concept we provide another naive method, one that is based on the major vote per item.
		This is yet another unpersonalized method since the value per item does not vary depending on the user.
		For any given user $u$, the predicted rating for an item $i$ is given by:
		\begin{equation}
			r_{u,i} = MR(i)
			\label{eq:majority}
		\end{equation}

		Where intuitively $MR(r_i)$ stands for the most common vote a given item $i$ has received throughout the review corpus.

	\subsection{Friends Average}
	\label{sec:friends_average}
		In this implementation we took the basic idea of the global average a step further into personalization.
		The main difference is that here we are interested in the given user's social circle's opinion.
		To incorporate that in the recommendation, we generate recommendations that consist of both the user's social circle as well as the global average rating of the items.

		To elaborate, we introduce a linear combination of the two values to deduce the predicted rating.
		The formula used for predicting a rating is given below:

		\begin{equation}
			r_{u,i} =
			\begin{cases}
				w_1 * FM(r_i) + w_2 * mean(r_i),	& \text{if $FM(r_i)$ is defined}.\\
				mean(r_i),							& \text{otherwise}.
			\end{cases}
			\label{eq:friends_average}
		\end{equation}

		Where $FM(r_i)$ stands for the average rating the friends of the given user $u$ have given item $i$.
		In case the value of $FM(r_i)$ is not defined (the users' friends have left no review to item $i$), a simple fall-back function is called upon; which uses the global average (as in \ref{sec:friends_average}).

		As seen in equation \ref{eq:friends_average} the weights $w_1$ and $w_2$ are parameters upon which optimization needs to be made in order to obtain the best score.
		The process followed to obtain the optimal weighting scheme is as follows.
		We sampled the test set $N=60$ times selecting a total of $s=test\_set\_size / 8$ samples at a time and then proceeded to estimate the RMSE score for each sample.
		In figure \ref{fig:weighted_rmse} we can see the range of the values the error metric gets for the various values of $w_1$ (x-axis).

		\begin{figure}[h b]
		\centering
		\includegraphics[width=\textwidth]{img/weighted_rmse.png}
		\caption[Friends average RMSE]{Error obtained for various combinations of $w_1, w_2$}
		\label{fig:weighted_rmse}
		\end{figure}

	% \subsection{Probabilistic Matrix Factorization (PMF)}
	% \label{subsec:pmf}
	% 	\todo{couple of paragraphs}

	% \subsection{Bayesian Probabilistic Matrix Factorization (Bayesian PMF)}
	% \label{subsec:bpmf}
	% 	\todo{couple of paragraphs}

	% \subsection{Social Recommendation using probabilistic matrix factorization (SoRec)}
	% \label{subsec:sorec}
	% 	Social Recommendation using probabilistic MF (SoRec) is a novel framework proposed by \citet{Ma08sorec:social} to tackle the recommendation problem.
	% 	As the method's name suggests, unlike the traditional MF implementations, it fuses users social circles with the user-item matrix, in order to compensate for the missing values.
	% 	The main intuition behind this methodology is based on the assumption that a user's social connections will affect the way the user behavior on the Web.

	% 	This approach is mainly influenced by \cite{Massa:2007:TRS:1297231.1297235}, in which a trust-aware collaborative filtering approach is proposed.
	% 	Experiments on a large real dataset conveyed by \citet{Massa:2007:TRS:1297231.1297235} reveal that this work does indeed increase the coverage (number of ratings that are predictable) without reducing accuracy.

	% 	The experiments conveyed in their work were aimed at revealing the improvement over various user sets.
	% 	In that direction they split the users on groups based on their activity (count of ratings left).
	% 	The results show a significant improvement in produced recommendations, more specifically they seem to outperform the state of the art systems the most, on sets of users with less overall ratings left.
	% 	This shows that this method helps the most for cases of data sparsity on the user-item rating matrix.

	% \subsection{Biased matrix factorization (BiasedMF)}
	% \label{sec:biasedmf}
	% 	\todo{couple of paragraphs}

	% \subsection{Regularized Singular Value Decomposition (RegSVD)}
	% \label{subsec:regsvd}
	% 	RegSVD is was introduced by A. Patelek \cite{citeulike:4563135} to solve the Netflix challenge, which makes it a highly interesting and relevant methodology to our work.
	% 	In this work, the author capitalizes on the performance of traditional SVD and proposes an extension with some added methodology inspired by other contestants' proposed solutions to the Netflix challenge.

	% 	For the use of SVD for the task of collaborative filtering, the methodology that is required is as follows: SVD is used on the ratings matrix with the missing values, then a k-means post processing takes place.
	% 	The RegSVD work proposes the extensions of the current pipeline by adding biases to the SVD, and for the post processing, a kernel ridge regression using a separate linear model for each item $i$.
	% 	Further, the work suggests reducing the parameter count.
	% 	Finally the predictors are combined using a simple linear regression.
	% 	This work yields a performance boost of $7.04\%$ on RMSE over the Netflix Cinematch algorithm and we therefore include it in our work as a good baseline method.

	\subsection{Text-enhanced Recommendation}
		\subsubsection{Topic Modeling}
		% "LDA hyperparameters" http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.149.1327&rep=rep1&type=pdf
		In our methods we make use of JGibbLDA\footnote{\url{http://jgibblda.sourceforge.net/}} \citep{Phan:2008:LCS:1367497.1367510, Biro:2009:LLD:1531914.1531922}, a Java implementation that also applies Gibbs sampling for parameter estimation and inference.
		In Section \ref{sec:text_enhanced_recommendation}, values of the Dirichlet parameters have been assumed to be known.
		These hyperparameters, however, significantly influence the behavior of the LDA model.

		Heuristically, sufficiently good model quality \cite{griffiths2004finding} has been reported for $\alpha = 50/K$ and $\beta = 0.1$.
		In our case, since our initial experiments assumed $K = 50$, we use $\alpha=1$ and $\beta=0.1$ which proved to provide good results and kept it after changing the number of topics to that $K = 25$.

		\subsubsection{Word Embeddings}
		To extract the word2vec similarity feature we make use of the official Google framework\footnote{\url{https://code.google.com/archive/p/word2vec/}}.
		There are some parameters that this implementation allows for tuning which affect the training speed and quality.
		The first of those parameters is for pruning the dictionary.
		Words that don't appear often throughout the vocabulary are deemed uninteresting, as especially in our context typos and garbage can be very frequent, for that reason we define a lower threshold for word appearance count below which the words are disregarded.
		We set the pruning count to 10.
		Another parameter is the size of the Neural Network layers, which correspond to the ``degrees'' of freedom the training algorithm has.
		Bigger size values require more training data, but can lead to better (more accurate) models. Reasonable values are in the tens to hundreds.
		We set the NN layers to 50 which results in vector representations of vectors of length 50.

	\subsection{Social-enhanced Recommendation}
		For this method, we make use of the previously mentioned Global Average feature and combine those with the social graph that we extracted using the methodology from \ref{sec:social_recommendation}.


	\subsection{Text-Social hybrid Recommendation}
		For the hybrid implementation we used the parameter tuning from the two methods mentioned prior and combined their results as mentioned in \ref{sec:hybrid_recommendation}.

	\subsection{Item Recommendation}
		For the task of item recommendation we experimented using a variety of learning to rank algorithms from the RankLib\footnote{\url{https://sourceforge.net/p/lemur/wiki/Home/}} package.
		Namely, the algorithms we experimented with for this task are: \textit{LambdaMART}, \textit{Random Forests} and \textit{Coordinate Ascent}.
    	We picked \textit{LambdaMART} to report on, a combination of \textit{LambdaRank} and \textit{MART}, which is a widely used learning to rank method that proved to be the most robust performer.
    	\textit{LambdaMART} described in ``Adapting Boosting for Information Retrieval Measures'' by \citet{Wu09learningto} is an ensemble method consisting of boosted regression trees (\textit{MART}) \cite{friedman2000greedy} in combination with \textit{LambdaRank} \cite{NIPS2006_2971}.
    	The tuning of this algorithm was set to have $1000$ trees with $10$ leaves each, the default setup of this method from the provider package.
    	We use ERR@10 as the error metric on training.

\section{Evaluation metrics}
\label{sec:evaluation_metrics}
	The task of recommendation is often divided into two subtasks, one is rating prediction which--as its name suggests--is the procedure of predicting a rating $\hat{r}_{i,j}$ for a user $i$ and item $j$.
	The second subtask is that of item recommendation, in which case, the objective is to present the user with a ranked list of items that are considered most relative to that user's taste.
	Those two tasks consider different metrics to measure their performance.
	The evaluation metrics used in this work adhere to the standards used widely in the recommender systems scenario.
	We divide the metrics into two categories, each of which depending on the use case.

	\subsection{Rating prediction}
	\label{subsec:rating_prediction}
		The accuracy for this task is measured by the ability to predict the rating a user would give to an item.
		Since the dataset used provides a numerical (star) value to the review of a user towards an item it is most common to assume the proximity of the prediction to the actual value as the most significant aspect of the evaluation.
		In our implementation the main evaluation metric we optimize for is Root Mean Squared Error ($RMSE$), the formula for $RMSE$ is given bellow:
		\begin{equation}
			RMSE = {\sqrt {\frac{1} {N}{\sum\limits_{i,j} {(r_{i,j} - \hat{r}_{i,j} } })^{2} } },
		\end{equation}
		where $r_{i,j}$ denotes the rating the rating user $i$ has given to item $j$, $\hat{r}_{i,j}$ denotes the predicted rating and $N$ is the number of predicted ratings.

		To accompany $RMSE$ we make use of another metric namely Mean Absolute Error (MAE), the formula for MAE is as follows:
		\begin{equation}
			MAE = \frac{1}{N}\sum_{i=1}^{N}|{r_{i,j} - \hat{r}_{i,j}}|,
		\end{equation}
		the notation being the same as described above.

		The ``stricter'' nature of $RMSE$ becomes apparent when comparing the two formulas, however it is one of the most common metrics used in this type of setup hence it is the one we optimized for.

	\subsection{Item recommendation}
	\label{subsec:evaluation_metrics_rating_prediction}
		The task of item recommendation differs from the rating prediction in the sense that in this case the objective is not to predict a rating for a given user-item pair, rather to generate a list of items for a given user which aims at placing the most relevant items to that user towards the top of the list.
		It becomes evident that the metrics presented in Subsection \ref{subsec:rating_prediction} are not applicable anymore since the objective of item recommendation is not to predict rating, rather to generate ranking lists.
		The most broadly used evaluation metric for used primarily in Information Retrieval applications whose task is to produce ranking lists of relevant documents, is Normalized Discounted Cumulative Gain ($NDCG$) \cite{Jarvelin:2002:CGE:582415.582418}.
		The task of item recommendation approximates that of Information Retrieval assuming that each user $u$ has a ``gain'' $g_{ui}$, from being recommended an item $i$.
		The average Discounted Cumulative Gain ($DCG$) for a list of items $J$ is defined as:
		\begin{equation}
			DCG = \frac{1}{N}\sum_{u=1}^{U}\sum_{i=1}^{I} \frac{g_{ui}}{max(1, \log_b i)}
		\end{equation}
		Where the logarithm base is a free parameter, typically set to $2$ (as is in this work).
		The normalized $DCG$ is given by:
		\begin{equation}
			NDCG = \frac{DCG}{DCG^*}
		\end{equation}
		Where $DCG^*$ stands for the ideal $DCG$.