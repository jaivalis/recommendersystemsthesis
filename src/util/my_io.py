from nltk.corpus import stopwords
from datetime import datetime
from time import time


def sentence_remove_stopwords(doc):
    ret = []
    for w in doc.split():
        if w not in stopwords.words('english'):
            ret.append(w)
    return ' '.join(ret)


def documents_to_dict(path):
    """
    Parses the documents to a dict (to RAM for speed)

    The file pointed by the path needs to follow the format:
        id [TAB] text

    :param path: path to corpus file
    :return: dict
    """
    ret = {}
    with open(path, 'r') as f:
        for line in f:
            tabs = line.split('\t')
            if len(tabs) > 1:
                doc_id = tabs[0]
                text = tabs[1]

                ret[doc_id] = text
    return ret


def file_exists(file_name):
    try:
        with open(file_name):
            return True
    except IOError:
        return False


def write_prediction_line(f, true_v, pred_v):
    f.write('{}\t{}\n'.format(true_v, pred_v))


def timestamp_str():
    """ Returns a timestamp that can be used for folder naming
    :return: string
    """
    return datetime.fromtimestamp(time()).strftime('%Y-%m-%d %H-%M-%S')


def parse_true_labels():
    ret = {}
    with open('../output/ratings.txt', 'r') as f:
        for line in f:
            parts = line.split()
            ret[parts[0] + '$' + parts[1]] = int(parts[2])
    return ret


def parse_relevant_pairs(f_path):
    """ Used for parsing user's friends and user's rated items
    :param f_path:
    :return:
    """
    ret = {}
    with open(f_path, 'r') as f:
        for line in f:
            parts = line.split('\t')
            user_id = parts[0]
            item_ids = parts[1].split(',')
            item_ids[-1] = item_ids[-1].strip()

            ret[user_id] = item_ids
    return ret


def parse_relevant_pairs_dense(f_path):
    """ Used for parsing user's friends and user's rated items
    :param f_path:
    :return:
    """
    ret = {}
    with open(f_path, 'r') as f:
        for line in f:
            parts = line.split('\t')
            user_id = parts[0]
            item_ids = parts[1].split(',')
            if len(item_ids) < 23:
                continue
            item_ids[-1] = item_ids[-1].strip()

            ret[user_id] = item_ids
    return ret


def format_letor_line(tl, u, v):
    """
    :param tl: true_label
    :param u: str user_id
    :param v: np array feature vector
    :return: str
    """
    ret = '{} qid:{} '.format(tl, u)
    f_index = 1
    for i in v:
        ret += '{}:{} '.format(f_index, i)
        f_index += 1
    return ret
