% !TEX root = main.tex

% > why each paper is related, how it's different
% > make background chapter (3-4 pages > 2), include CF MF
% > related work (2-3 pages) why it's related why it's different.
% > first sentence in a paragraph makes a point, the rest backs it up.
\chapter{Background}
\label{ch:background}
    The purpose of this Chapter is to provide background knowledge about recommender systems.
    To achieve that, some of the fundamentals that have been developed in this research area over the course of the last years will be briefly introduced.

    \section{Definition of Recommender Systems}

        One of the first definitions for recommender systems was presented by Paul Resnick and Hal R. Varian in 1997.
        In their work they state that ``in a typical recommender system, people provide recommendations as inputs, which the system then aggregates and directs to appropriate recipients'' \citep{resnick97a}.

        Recommender systems (RSs) are typically classified into non-personalized and personalized methods.
        As their names suggest, the former do not consider the individual user and the latter build on the so called \textit{user profile} in order to make recommendations.
        Personalized recommenders are further divided into persistent (or long term) and ephemeral (or short term), \cite{Mizzaro2002}.
        Those notions are illustrated in Figure \ref{fig:recommendation_methods}.

        \begin{figure}[h]
        \centering
        \includegraphics[width=\textwidth]{img/recommendation_hierarchy.png}
        \caption[Recommendation methods]{Hierarchy of recommender systems per personalization.}
        \label{fig:recommendation_methods}
        \end{figure}

        Some examples of non-personalized methods consist of wisdom of the crowds oriented methods such as ``most-popular'' items \cite{anil:nonpersonalized}.
        Obviously this type of recommendation is trivial and does not involve machine learning.

        In this work we are interested in the personalized scenario.
        Briefly put, personalized recommender systems output recommendations that are targeted at specific individuals based on their feedback, expressed explicitly or implicitly.
        This feedback forms the \textit{user profile}, which is then used to generate recommendations.
        An example of information that can be included in a \textit{user profile} is user demographics \cite{Lu:2015:EUB:2766462.2767806}.

        The formal definition of a recommendation system can be expressed as follows.
        Given a set of users, a set of items, and reviews that characterize the users' opinions towards a set of items, the system aims at pointing a user towards new, not-yet-experienced items that may be relevant to the users current task \cite{Ricci2011}.

    \section{Why Recommender Systems?}
        As the CEO of Amazon put it, ``If I have 3 million customers on the Web, I should have 3 million stores on the Web'' \citep{Schafer01e-commercerecommendation}.
        J. Bezos, stresses that the effect a successful recommendation system has over a web service can dramatically improve the customer experience and therefore the service's overall success.

        The general purpose of such a system is to satisfy the customer by providing them with what they want to see when browsing through a service.
        Research indicates that it is much more cost-efficient to keep a current customer than to find a new one \citep{slater1992superior}, therefore it is important to keep the user experience satisfactory by showing them content related to their interests.
        RSs help in providing personalized engaging content for services such as web stores with low overhead, therefore a lot of research has been focused on this area.

    \section{Categorization of Recommender Systems}
        There are various ways to categorize recommender systems.
        In this section we shortly present a taxonomy that is relevant to the way we approach the problem.

        \subsection{Content-Based Filtering}
        \label{subsec:cbf}
            We start off with the simplest form of filtering, Content-Based Filtering (CBF).
            In CBF user activity is constantly monitored, and the system provides the user with items that match items that she has left positive reviews for, in the past.
            This type of recommendation is often applied for suggesting documents, web pages and mostly text-based items.

            CBF is currently not very widely used since it adheres to a specific set of features and tends to overspecialize, due to the fact that previously rated items by individual users are deemed the most important resource for recommendation \citep{Montaner:2003:TRA:640471.640491, Adomavicius05towardthe}.

            A positive remark for this type of system is the simplicity of implementation, since in order to generate a user profile it is sufficient to traverse only the items rated by the specific user.

        \subsection{Collaborative Filtering}
        \label{subsec:cf}
            A method that is the most mature and most widely applied for recommender systems is Collaborative Filtering (CF) \citep{burke2002hybrid}.
            CF is a term first coined by the developers of Tapestry \cite{Goldberg:1992:UCF:138859.138867}, the first ever recommender system.
            In this approach, recommendations are again deduced by user-item ratings, however here the origin of the recommendations are users that are evaluated as similar to the recommendation subject.
            Contrary to the systems mentioned above, CF requires a user profile that is formed by accumulated information regarding the user.
            This user profile consists of a vector of item ratings that associate the given user with a set of items from the item pool.
            These ratings can contain any type of information but are usually limited to a boolean value (like/dislike) or in a more fine-grained resolution, a rating (e.g. a number in range of [1, 5]).
            An example of a platform that uses this method is Amazon\footnote{\url{http://www.amazon.com}}, with a method called item-to-item collaborative filtering \cite{Linden:2003:ARI:642462.642471}.
            This algorithm produces high quality real-time recommendations and manages well with the scaling required in a big datasets such as that of Amazon
            It produces recommendations under the spectrum of ``users who bought item \textit{x}, also bought \textit{y}'', in an attempt to direct users to a new purchase based on what other users found useful to combine item \textit{x} with.

            The main difference of CF compared to the CBF is that the system is more versatile since it can offer a bigger variety of recommendation items by deducing user similarities and predicting user preferences by looking at other users' past activity.

            In the CF scenario, the recommendation consists of the following steps.
            First, determining a user neighborhood which is usually calculated by observing user ratings \citep{Montaner:2003:TRA:640471.640491, burke2002hybrid}.
            Second, determining a set of recommendations based on this neighborhood by extracting a subset that adheres to a given user profile the most.
            This is one of the most effective out-of-the-box solutions to recommendation to this day.
            % An issue that is common in CF implementation, is the so called cold-start problem which stems in the fact that items that have not yet been rated do not fit in any profile until they have been.
            % Some heuristics have been introduced to tackle this problem as well.

        \subsection{Hybrid Methods}
            Research on RS has matured enough to show that strictly adhering to the methods introduced previously comes with a cost that we illustrated in Sections \ref{subsec:cbf} and \ref{subsec:cf}.
            Therefore, to tackle such issues various hybrid methods have been introduced which combine the best of two worlds in order to improve performance.
            Such is the nature of most of the methods that we will introduce in detail in Chapter \ref{ch:related_work}.

    \section{Natural Language Processing}
    \label{sec:lda}
        As stated in Chapter \ref{ch:introduction}, part of our proposed methods depend on processing text that accompanies user reviews.
        In order to leverage the text in our data we employ two methods, topic modeling and word embeddings.
        In machine learning and natural language processing, a topic model is a statistical model for discovering the abstract topics that occur in a collection of documents.
        A \textit{topic} is a distribution over a fixed vocabulary.

        \subsection{Topic Modeling}
        We can now formulate the problem into that of extracting topics from all the user generated text and then assigning users to those topics in a stochastic manner.
        Topic modeling has proven \cite{DBLP:conf/ijcai/AbelGHT13, diao-jiang:2013:EMNLP} to be a good way to encode text to enhance recommendation, in this work we will investigate how well it performs compared to word embedding.
        We choose to use Latent Dirichlet Allocation (LDA), a state-of-the-art solution introduced by \citep{Blei2001LatentDA}.
        The hypothesis LDA makes is that each document consists of a probabilistic mixture of multiple topics.
        Each topic has a vocabulary of tokens that it produces with a given probability.
        LDA is most easily described by its generative process, the imaginary random process by which the model assumes the documents arose.
        Furthermore, each term of each document, itself has a marginal contribution to the mixture since it can also be expressed as a mixture of the same topics as follows: from the collection of words that is each document $D$ the model describes how each document arose.
        First the $K$ priors are set which represent the topic distributions as multinomials containing $V$ elements each, where $V$ is the number of tokens in the corpus.
        Let $\beta_i$ represent the multinomial for the $i$-th topic, where the size of $\beta_i$ is $V:|\beta_i|=V$.
        Given these distributions, the LDA generative process is defined as follows:

        For each document:
        \begin{itemize}
            \item randomly choose a distribution over topics (a multinomial of length $K$) $\theta_d$
            \item for each word in the document:
                \begin{itemize}
                    \item Probabilistically draw one of the $K$ topics from distribution $t_i$, say topic $\beta_j$
                    \item Probabilistically draw one of the $V$ words from $\beta_j$
                \end{itemize}
        \end{itemize}

        This generative model emphasizes that documents are comprised of multiple topics.
        The first step of the process reflects that each document contains topics in different proportions.
        The second step shows that each word in the document is drawn from one of the $K$ topics in proportion to the document's distribution over topics as determined in the first step.
        To be noted here that this model does not make any assumptions about the order of the words, otherwise known as the \textit{bag-of-words assumption}.

        Sampling-based algorithms attempt to collect samples from the posterior to approximate it with an empirical distribution.
        The most commonly used sampling algorithm for topic modeling--and the one we use--is Gibbs sampling, where a Markov chain is constructed, each dependent on the previous-whose limiting distribution is the posterior.
        Further details on Gibbs sampling and why we picked are presented in Chapter \ref{ch:methodology}.

    \subsection{Word Embeddings}
        The second method we utilize to vectorize text is \textit{word embedding}, a well studied and reported on method among machine learning and data mining techniques \citep{conf/interspeech/MesnilHDB13,TACL809,NIPS2014_5477}.
        Word embedding is the collective name for a set of language modeling and feature learning techniques in natural language processing where words or pieces of text are mapped to dense vectors of real numbers.
        The term originates from the mathematical concept of embedding from a space with one dimension per word, to a continuous vector space of much lower dimension.

        \citet{DBLP:journals/corr/abs-1301-3781} introduced a popular \citep{DBLP:journals/corr/GoldbergL14, DBLP:journals/corr/Rong14} framework which solves the problem of text vectorization by utilizing the context in which words appear in text, called word2vec.
        Word2vec comprises of two distinct models: \textit{continuous bag-of-words} (\textit{CBOW}) and \textit{continuous skip-gram}.
        In the \textit{continuous skip-gram} architecture, the model uses the current word to predict the surrounding window of context words.
        According to the authors \textit{CBOW} is faster while skip-gram is slower but more efficient when it comes to infrequent words.

        Word2vec uses a single hidden layer fully connected neural network.
        The input and output layers is set to have as many neurons as there are layers in the training vocabulary.
        The hidden layer size, defines the dimensionality of the resulting word vectors.
        The size of the output layer is the same as the input layer.

    We have layed out some of the basic methodology introduced to address the task of recommendation.
    Chapter \ref{ch:related_work} shows some state-of-the art methods that motivated our work.