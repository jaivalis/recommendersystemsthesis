import sys
import types
import cPickle

from pymongo import MongoClient
from bson.binary import Binary
from numpy import ndarray
import numpy as np

from util.my_math import weighted_average

reload(sys)
sys.setdefaultencoding('utf8')


class DB:
    """ Anything regarding IO with the database is in this class. """

    def __init__(self):
        client = MongoClient('localhost', 27017)
        self.yelp = client['yelp']

        self.business_collection = self.yelp['business']
        user_id_cursor = self.yelp['user_subset'].find({'$and': [
            {'$where': 'this.friends.length>2'},
            {'review_count': {'$gt': 2}}
        ]}, {'_id': 0, 'user_id': 1})

        self.user_list = list(user_id_cursor)

    # simple get_table_list()
    def get_user_id_list(self):
        return self.user_list

    def get_business_id_list(self):
        return list(self.yelp['business_subset'].find({}, {'business_id': 1}))

    def get_user_list(self):
        return list(self.yelp['user_subset'].find({}))

    def get_review_list(self):
        return list(self.yelp['review_subset'].find({}))

    def get_business_list(self):
        return list(self.yelp['business_subset'].find({}))

    def get_review_text_list(self):
        return list(self.yelp['review_subset'].find({}, {'text': 1}))

    def get_user_review_text_list(self):
        return list(self.yelp['user_review_text'].find({}, {'text': 1}))

    def get_business_review_text_list(self):
        return list(self.yelp['business_review_text'].find({}))

    def get_business_review_text_12_list(self):
        return list(self.yelp['business_review_text_12'].find({}))

    def get_business_review_text_45_list(self):
        return list(self.yelp['business_review_text_45'].find({}))

    def get_user_review_text_12_list(self):
        return list(self.yelp['user_review_text_12'].find({}))

    def get_user_review_text_45_list(self):
        return list(self.yelp['user_review_text_45'].find({}))
    # /simple get_table_list()

    def get_review_text(self, u_id, b_id):
        ret = ''
        l = list(self.yelp['review_subset'].find(({'$and': [{'user_id': u_id}, {'business_id': b_id}]}), {'text': 1}))
        for r in l:
            ret += r['text'].strip() + ' '
        return ret.strip()

    def get_user(self, u_id):
        return self.yelp['user'].find_one({'user_id': u_id})

    def get_user_reviewed_business_ids(self, u_id):
        return list(self.yelp['review_subset'].find({'user_id': u_id}, {'business_id': 1, 'stars': 1}))

    def get_distinct_business_ids(self):
        return self.yelp['review_subset'].distinct('business_id')

    def get_distinct_user_ids(self):
        return self.yelp['review_subset'].distinct('user_id')

    def get_true_label(self, u_id, b_id):
        """ Returns the rating a user has given a business """
        return list(self.yelp['review_subset'].find({'user_id': u_id, 'business_id': b_id}, {'stars': 1}))[0]['stars']

    def get_average_stars(self, b_id):
        """ Returns the average rating for a given business
        :param b_id: business id
        :return: average rating for a given business
        """
        ret = self.yelp['review_subset'].aggregate([
            {'$match': {'business_id': b_id}},
            {'$group': {'_id': 'null', 'avg_stars': {'$avg': '$stars'}}}
        ])
        return ret.next()['avg_stars']

    def get_friends(self, u_id):
        """ Returns a list with the ids of ui_d users' friends
        :param u_id: user id
        :return: list with the ids of ui_d users' friends
        """
        assert isinstance(u_id, types.UnicodeType)

        res = self.yelp['user_subset'].find({'user_id': u_id}, {'friends': 1})
        return res[0]['friends']

    def get_friends_average_stars(self, friends_ids, b_id):
        """ Returns the average ratings of a business given some user ids
        :param friends_ids: list of user ids
        :param b_id: business id
        :return: float rating (None if no ratings exist)
        """
        stars = 0
        common = 0

        relevant_reviews = self._get_users_reviews(friends_ids, b_id)

        for rr in relevant_reviews:
            stars += float(rr['stars'])
            common += 1.0

        if common == 0:
            return None
        return float(stars) / common

    def get_user_reviews(self, u_id):
        """ Returns the reviews of a given user
        :param u_id: user id
        :return: a mongodb cursor to the reviews
        """
        return self.yelp['review_subset'].find({'user_id': u_id})

    def get_business_reviews(self, b_id):
        """ Returns the reviews of a given user
        :param b_id: business id
        :return: a mongodb cursor to the reviews
        """
        return self.yelp['review_subset'].find({'business_id': b_id})

    def get_user_reviews_stars_in(self, u_id, stars):
        """ Returns the reviews with stars equal to the given parameter
        :param u_id: user id
        :param stars: list star threshold
        :return: a mongodb cursor to the reviews
        """
        return self.yelp['review_subset'].find({'user_id': u_id, 'stars': {'$in': stars}})

    def get_business_reviews_stars_in(self, b_id, stars):
        """ Returns the reviews with stars equal to the given parameter
        :param b_id: business id
        :param stars: list star threshold
        :return: a mongodb cursor to the reviews
        """
        return self.yelp['review_subset'].find({'business_id': b_id, 'stars': {'$in': stars}})

    def get_business_review_text(self, b_id):
        """ Returns the reviews' aggregate text
        :param b_id: user id
        :return: str
        """
        res = self.yelp['business_review_text'].find_one({'item_id': b_id}, {'text': 1})
        if res is None:
            return None
        return res['text']

    def get_user_review_text(self, u_id):
        """ Returns the reviews' aggregate text
        :param u_id: user id
        :return: str
        """
        res = self.yelp['user_review_text'].find_one({'user_id': u_id}, {'text': 1})
        if res is None:
            return None
        return res['text']

    def get_user_review_text_12(self, u_id):
        """ Returns the reviews with 1 or 2 stars
        :param u_id: user id
        :return: str
        """
        res = self.yelp['user_review_text_12'].find_one({'user_id': u_id}, {'text': 1})
        if res is None:
            return None
        return str(res['text'])

    def get_user_review_text_45(self, u_id):
        """ Returns the reviews with 4 or 5 stars
        :param u_id: user id
        :return: str
        """
        res = self.yelp['user_review_text_45'].find_one({'user_id': u_id}, {'text': 1})
        if res is None:
            return None
        return str(res['text'])

    def get_business_review_text_per_rating(self, b_id, ratings):
        """ Returns the reviews with 4 or 5 stars
        :param b_id: business id
        :return: str
        """
        table_name = 'business_review_text_12' if ratings == [1, 2] else 'business_review_text_45'
        res = self.yelp[table_name].find_one({'item_id': b_id}, {'text': 1})
        if res is None:
            return None
        return res['text']

    def get_user_review_text_stars_in(self, u_id, stars):
        """ Returns the reviews with stars equal to the given parameter
        :param u_id: user id
        :return: a mongodb cursor to the reviews
        """
        ret = ''
        for r in self.yelp['review_subset'].find({'user_id': u_id, 'stars': {'$in': stars}}, {'text': 1}):
            ret += r['text'] + ' '
        return ret

    def get_business_review_text_stars_in(self, b_id, stars):
        """ Returns the reviews with stars equal to the given parameter
        :param b_id: business id
        :return: a mongodb cursor to the reviews
        """
        ret = ''
        for r in self.yelp['review_subset'].find({'business_id': b_id, 'stars': {'$in': stars}}, {'text': 1}):
            ret += r['text'] + ' '
        return ret

    def get_user_reviewed_business_id_set(self, u_id):
        ret = set()
        for o in list(self.yelp['review_subset'].find({'user_id': u_id}, {'business_id': 1})):
            ret.add(o['business_id'])
        return ret

    # private:
    def _get_user_business_reviews(self, u_id, b_id):
        """ Returns the reviews the user has left on a given business
        :param u_id: user id
        :param b_id: business id
        :return: the reviews the user has left on a given business
        """
        res = self.yelp['review_subset'].find({'$and': [
            {'user_id': u_id},
            {'business_id': b_id}
        ]})
        return res

    def _get_users_reviews(self, users, b_id):
        """ Returns the reviews the user has left on a given business
        :param users: list containing user ids
        :param b_id: business id
        :return: the reviews the user has left on a given business
        """
        res = self.yelp['review_subset'].find({'$and': [
            {'business_id': b_id},
            {'user_id': {'$in': users}}
        ]})
        return res

    def get_business_in_categories(self, cats):
        return self.yelp['business_subset'].find({'categories': {'$in': cats}})

    def set_w2v_vector(self, table_name, obj_id, key_name, attribute_name, numpy_array):
        assert isinstance(numpy_array, ndarray)
        return self.yelp[table_name].update_one({key_name: obj_id},
                                                {'$set':
                                                {attribute_name: Binary(cPickle.dumps(numpy_array, protocol=2))}})

    def get_w2v_vector(self, table_name, obj_id, key_name, topic_count):
        tmp = self.yelp[table_name].find_one({key_name: obj_id}, {'w2v@'+str(topic_count): 1})
        if tmp is not None:
            ret = cPickle.loads(tmp['w2v@'+str(topic_count)])
            assert isinstance(ret, ndarray)
            if ret.size != 0:
                return ret
        return None

    def get_lda_pd_vector(self, table_name, id_):
        key = 'user_id' if table_name.startswith('user_review') else 'item_id'
        tmp = self.yelp[table_name].find_one({key: id_}, {'LDA_pd': 1})
        if tmp is not None:
            ret = cPickle.loads(tmp['LDA_pd'])
            assert isinstance(ret, ndarray)
            if ret.size != 0:
                return ret
        return None

    def fetch_social_vector(self, document):
        if document is None:
            return np.zeros(50)
        try:
            ret = cPickle.loads(document['social_w2v@50'])
        except KeyError:  # No friends :(
            return np.zeros(50)
        return ret

    def get_social_vector(self, u_id, ratings=[1, 2, 3, 4, 5]):
        if ratings == [1, 2]:
            table_name = 'user_review_text_12'
            doc = self.yelp[table_name].find_one({'user_id': u_id}, {'social_w2v@50': 1})
        elif ratings == [4, 5]:
            table_name = 'user_review_text_45'
            doc = self.yelp[table_name].find_one({'user_id': u_id}, {'social_w2v@50': 1})
        else:
            doc1 = self.yelp['user_review_text_12'].find_one({'user_id': u_id}, {'social_w2v@50': 1})
            doc2 = self.yelp['user_review_text_45'].find_one({'user_id': u_id}, {'social_w2v@50': 1})
            return self.fetch_social_vector(doc1) + self.fetch_social_vector(doc2)

        if doc is not None:
            try:
                ret = cPickle.loads(doc['social_w2v@50'])
            except KeyError:  # No friends :(
                return None
            assert isinstance(ret, ndarray)
            if ret.size != 0:
                return ret
        return None
